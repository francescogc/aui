import './helpers/mock-require';
import { afterMutations, removeLayers } from './helpers/all';
import $ from 'jquery';

let fixtureElement;

/**
 * Sets the el's innerHTML to '' and executes the next callback after any
 * DOM mutation handlers (e.g., skate's detached callbacks) have had a
 * chance to run.
 */
function clearContents(el, next) {
    afterMutations(() => {
        el.innerHTML = '';
        afterMutations(() => {
            if (next) {
                next();
            }
        })
    });
}

beforeEach(function (done) {
    fixtureElement = document.getElementById('test-fixture');
    if (!fixtureElement) {
        fixtureElement = document.createElement('div');
        fixtureElement.id = 'test-fixture';
        document.body.appendChild(fixtureElement);
    }
    clearContents(fixtureElement, done);
});

afterEach(function (done) {
    if (setTimeout.clock) {
        setTimeout.clock.restore();
    }

    // unbind some test-specific handlers that might've not been cleaned up
    $(window).off('.aui-test-suite');
    $(document).off('.aui-test-suite');

    clearContents(fixtureElement, function () {
        removeLayers();
        // give Tether some time to update before killing the DOM
        // Needed for: https://connect.microsoft.com/IE/feedback/details/829392/
        // IE 10 & 11: Calling getBoundingClientRect on an HTML Element that has not
        // been added to the DOM causes "Unspecified error"
        afterMutations(function () {
            $('body, html').css('overflow', '');
            done();
        }, 50); // see 'elements' helpers for its delay.
    });
});
