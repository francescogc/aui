(function() {
    // Small AMD shim to test components that have been AMDified. We don't check
    // define.amd but most libs do. If they don't check then they will end up
    // calling this and may not export a global. This may cause problems, but since
    // we don't have a good way to mock ES2015 modules yet, this will suffice.
    const defines = {};
    window.define = function (name, deps, func) {
        if (typeof name === 'string') {
            defines[name] = func || deps;
        }
    };
    window.amdRequire = function (names, func) {
        func.apply(this, names.map(name => defines[name]()));
    };

}());
