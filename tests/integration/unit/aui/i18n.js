import i18n from '@atlassian/aui/src/js/aui/i18n';

describe('aui/i18n', function () {
    it('globals', function () {
        expect(AJS.I18n.toString()).to.equal(i18n.toString());
    });

    it('return key test', function () {
        expect(i18n.getText('test.key')).to.equal('test.key');
    });

    describe('returns the value for a defined key', function () {
        beforeEach(function () {
            i18n.keys['test.key'] = 'This is a Value';
            i18n.keys['test.formatting.key'] = 'Formatting {0}, and {1}';

        });

        afterEach(function () {
            delete i18n.keys['test.key'];
            delete i18n.keys['test.formatting.key'];
        });

        it('with no formatting', function () {
            expect(i18n.getText('test.key')).to.equal('This is a Value');
        });

        it('with formatting', function () {
            expect(i18n.getText('test.formatting.key', 'hello', 'world')).to.equal('Formatting hello, and world');
        });
    });
});
