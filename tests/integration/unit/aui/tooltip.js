import '@atlassian/aui/src/js/aui/tooltip';
import $ from '@atlassian/aui/src/js/aui/jquery';
import { hover } from '../../helpers/all';

describe('aui/tooltip', function () {
    const TIPSY_DELAY = 10;

    let clock;
    let ctx;
    let $el;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        ctx = document.body;
    });

    afterEach(function () {
        clock.restore();
    });

    function visibleTipsies () {
        return $(ctx).find('.tipsy').filter(':visible');
    }

    function runSharedTooltipTests () {
        it('will show up when delayed', function () {
            expect(visibleTipsies().length).to.equal(0);

            hover($el);

            clock.tick(1);
            expect(visibleTipsies().length).to.equal(0);

            clock.tick(TIPSY_DELAY);
            const $tipsies = visibleTipsies();
            expect($tipsies.length).to.equal(1);

            const position = $tipsies.position();
            expect(position.left).to.not.equal(0);
            expect(position.top).to.not.equal(0);
        });

        it('will not show when they are delayed and if the underlying element is gone', function () {
            expect(visibleTipsies().length).to.equal(0);

            hover($el);

            $el.remove();

            clock.tick(TIPSY_DELAY);
            expect(visibleTipsies().length).to.equal(0);
            expect($('.tipsy').length).to.not.be.defined;
        });

        it('trigger has aria-describedby should be on the link with correct id', function () {
            hover($el);
            clock.tick(TIPSY_DELAY);
            const tipsyId = visibleTipsies()[0].id;
            expect($el.attr('aria-describedby')).to.equal(tipsyId);
        });

        it('tooltip should have an id', function () {
            hover($el);
            clock.tick(TIPSY_DELAY);
            const tipsyId = visibleTipsies()[0].id;
            expect(tipsyId).to.not.equal('');
        });
    }

    describe('on a <div>', function () {
        beforeEach(function () {
            $el = $('<div title="my element">Element</div>');
            $(ctx).append($el);
            $el.tooltip({
                delayIn: TIPSY_DELAY
            });
        });

        afterEach(function () {
            $el.remove();
            $(ctx).find('.tipsy').remove();
        });

        runSharedTooltipTests();

        it('tooltips can be destroyed with the "destroy" option', function () {
            $el.tooltip('destroy');
            clock.tick(TIPSY_DELAY);
            expect(visibleTipsies().length).to.equal(0);
            expect($('.tipsy').length).to.not.be.defined;
        });
    });

    describe('live tooltips', function () {
        let whereBindingWasStored = document.createElement('div');

        afterEach(function () {
            $el.remove();
            $(ctx).find('.tipsy').remove();
        });

        beforeEach(function () {
            $el = $('<div class="has-tooltip" title="my element">Element</div>');
            $(ctx).append($el);

            $(whereBindingWasStored).tooltip({
                delayIn: TIPSY_DELAY,
                live: '.has-tooltip'
            });
        });

        function runLiveTooltipDestroyTests() {
            it('can be destroyed when "destroy" called on the element they were bound to', function () {
                $(whereBindingWasStored).tooltip('destroy');

                hover($el);

                clock.tick(TIPSY_DELAY);
                expect(visibleTipsies().length).to.equal(0);
            });

            it('cannot be destroyed when "destroy" called on different element than bound to ', function () {
                $(ctx).tooltip('destroy');

                hover($el);

                clock.tick(TIPSY_DELAY);
                expect(visibleTipsies().length).to.equal(1);
            });
        }

        runSharedTooltipTests();
        runLiveTooltipDestroyTests();

        describe('on a dynamically added element', function () {
            beforeEach(function () {
                $el = $('<div class="has-tooltip" title="my element-2">Element 2</div>');
                $(ctx).append($el);
            });

            runSharedTooltipTests();
            runLiveTooltipDestroyTests();
        });
    });

    describe('on svgs', function () {
        const TIPSY_MARGIN = 50;
        let $rect;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            const svgHtml = '<svg width="300" height="300">' +
                '<rect id="svg-rect" x="50" y="50" width="50" height="50" fill="red" />' +
                '</svg>';
            $el = $(svgHtml);
            $(document.getElementById('test-fixture')).append($el);
            $rect = $('rect');

            $rect.tooltip({
                delayIn: 0,
                title: function () { return 'an svg element'; }
            });
        });

        afterEach(function () {
            $el.remove();
            $(ctx).find('.tipsy').remove();
        });

        it('will position correctly when attached to an svg element', function () {
            hover($rect);
            clock.tick(1);

            const $tipsies = visibleTipsies();
            const tipsyPosition = $tipsies.position();
            const rectPosition = $rect.position();
            const rectSize = $rect[0].getBoundingClientRect().width

            expect($tipsies.length).to.equal(1);
            expect(tipsyPosition.left).to.be.within(rectPosition.left - TIPSY_MARGIN, rectPosition.left + rectSize + TIPSY_MARGIN);
            expect(tipsyPosition.top).to.be.within(rectPosition.top + rectSize, rectPosition.top + rectSize + TIPSY_MARGIN);
        });
    });
});
