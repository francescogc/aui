import '@atlassian/aui/src/js/aui/dropdown2';
import $ from '@atlassian/aui/src/js/aui/jquery';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import WebComponentDropdown from './dropdown2/dropdown2-test-webcomponent-helper';
import { runDropdown2BehaviouralTests } from './dropdown2/behavioural-tests';
import { afterMutations, click, fixtures, focus, hover, pressKey } from '../../helpers/all';

describe('Dropdown – web component', function () {
    function createItem(html) {
        return skate.init(fixtures({ item: html }).item);
    }

    runDropdown2BehaviouralTests(WebComponentDropdown);

    describe('aui-item-link', function () {
        it('passes an href through to the link', function () {
            const href = '#foo';
            const item = createItem(`<aui-item-link href="${href}">The link</aui-item-link>`);
            const link = item.querySelector('a');
            expect(link).to.not.be.null;
            expect(link.getAttribute('href')).to.equal(href);
        });

        it('passes an ID through to a link', function () {
            const id = 'something-or-other';
            const item = createItem(`<aui-item-link item-id="${id}">The link</aui-item-link>`);
            const link = item.querySelector('a');
            expect(link.getAttribute('id')).to.equal(id);
        });
    });

    describe('aui-item-checkbox', function () {
        it('passes an ID through to the link', function () {
            const id = 'something-or-other';
            const item = createItem(`<aui-item-checkbox item-id="${id}">The link</aui-item-checkbox>`);
            const link = item.querySelector('span');
            expect(link.getAttribute('id')).to.equal(id);
        });
    });

    describe('aui-item-radio', function () {
        it('passes an ID through to the link', function () {
            const id = 'something-or-other';
            const item = createItem(`<aui-item-radio item-id="${id}">The link</aui-item-radio>`);
            const link = item.querySelector('span');
            expect(link.getAttribute('id')).to.equal(id);
        });
    });

    describe('with asynchronous content -', function () {
        var dropdown;
        var trigger;
        var server;
        var clock;

        beforeEach(function () {
            server = sinon.fakeServer.create();
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            server.restore();
            clock.restore();
        });

        function makeFixturePointingTo(src, json, returnCode) {
            var jsonString = (typeof json === 'string' ? json : JSON.stringify(json));
            server.respondWith(src, [
                returnCode || 200,
                {'Content-Type': 'application/json'},
                jsonString
            ]);

            return fixtures({
                trigger: `
                    <a href="#" aria-owns="dd-web-component-async" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Async dropdown trigger</a>
                `,
                dropdown: `
                    <aui-dropdown-menu id="dd-web-component-async" src="${src}">
                    </aui-dropdown-menu>
                `
            });
        }

        function getLoadingDiv(dropdown) {
            return dropdown.querySelector('.aui-dropdown-loading');
        }

        function getErrorDiv(dropdown) {
            return dropdown.querySelector('.aui-dropdown-error');
        }

        describe('loading state -', function () {
            beforeEach(function () {
                var simpleResponse = [
                    {type: 'section', label: 'Projects', items: []}
                ];

                var constructed = makeFixturePointingTo('/simple', simpleResponse);

                dropdown = skate.init(constructed.dropdown);
                trigger = skate.init(constructed.trigger);
            });

            it('hover starts loading on background', function () {
                expect(getLoadingDiv(dropdown)).to.not.exist;
                hover(trigger);
                expect(getLoadingDiv(dropdown)).to.exist;
                expect(getLoadingDiv(dropdown)).to.not.be.visible;
            });

            it('click shows the loading', function () {
                expect(getLoadingDiv(dropdown)).to.not.exist;
                click(trigger);
                expect(getLoadingDiv(dropdown)).to.exist;
                expect(getLoadingDiv(dropdown)).to.be.visible;
            });

            it('focus + SPACE -> show the loading', function () {
                focus(trigger);
                expect(getLoadingDiv(dropdown)).to.not.exist;
                pressKey(keyCode.SPACE, null, trigger);
                expect(getLoadingDiv(dropdown)).to.exist;
                expect(getLoadingDiv(dropdown)).to.be.visible;
            });

            it('response hide the loading', function () {
                click(trigger);
                expect(getLoadingDiv(dropdown)).to.be.visible;
                server.respond();
                expect(getLoadingDiv(dropdown)).to.not.exist;
            });

            it('not reload while in loading state', function () {
                server.autoRespond = true;
                server.autoRespondAfter = 1000;
                click(trigger);
                clock.tick(900);
                expect(getLoadingDiv(dropdown)).to.be.visible;

                // make sure second click doesn't trigger reload and extends waiting time
                click(trigger);
                clock.tick(100);
                expect(getLoadingDiv(dropdown)).to.not.exist;
            });
        });

        describe('error state -', function () {
            describe('invalid response -', function () {
                beforeEach(function () {
                    var invalidJsonResponse = '<script>alert("I don\'t think this is JSON")</script>';
                    var constructed = makeFixturePointingTo('/invalid', invalidJsonResponse);

                    dropdown = skate.init(constructed.dropdown);
                    trigger = skate.init(constructed.trigger);
                });

                it('clicking on the trigger opens a dropdown that has an error display', function () {
                    click(trigger);
                    expect(getErrorDiv(dropdown)).to.not.exist;
                    server.respond();
                    expect(getErrorDiv(dropdown)).to.be.visible;
                });

                it('can trigger reload again', function () {
                    click(trigger);
                    server.respond();
                    expect(getLoadingDiv(dropdown)).to.not.exist;
                    expect(getErrorDiv(dropdown)).to.be.visible;

                    // click again to close dropdown
                    click(trigger);
                    expect(getErrorDiv(dropdown)).to.be.not.visible;

                    // trigger reload
                    click(trigger);
                    expect(getLoadingDiv(dropdown)).to.be.visible;
                });

                it('not trigger reload until after error is visible to user', function () {
                    hover(trigger);
                    server.respond();
                    expect(getErrorDiv(dropdown)).to.exist;
                    expect(getErrorDiv(dropdown)).to.be.not.visible;

                    // not trigger reload because error message is still hidden
                    hover(trigger);
                    expect(getLoadingDiv(dropdown)).to.not.exist;

                    // show the error
                    click(trigger);
                    expect(getErrorDiv(dropdown)).to.be.visible;

                    // not reload while dropdown is still opened
                    hover(trigger);
                    expect(getLoadingDiv(dropdown)).to.not.exist;

                    // click again to close dropdown and hover to trigger reload
                    click(trigger);
                    hover(trigger);
                    expect(getLoadingDiv(dropdown)).to.exist;
                });
            });

            describe('invalid response code -', function () {
                beforeEach(function () {
                    var simpleResponse = [
                        {type: 'section', label: 'Projects', items: []}
                    ];

                    var constructed = makeFixturePointingTo('/simple', simpleResponse, 201);

                    dropdown = skate.init(constructed.dropdown);
                    trigger = skate.init(constructed.trigger);
                });

                it('201 is considered as an invalid response', function () {
                    click(trigger);
                    expect(getErrorDiv(dropdown)).to.not.exist;
                    server.respond();
                    expect(getErrorDiv(dropdown)).to.be.visible;
                });
            });
        });

        describe('and json repsonse of two sections', function () {
            beforeEach(function () {
                var twoSectionResponse = [
                    {type: 'section', label: 'Projects', items: [
                        {type: 'link', href: '#aui', content: 'AUI'},
                        {type: 'link', href: '#design-platform', content: 'Design Platform'},
                        {type: 'link', href: '#children', for: 'child', content: 'Children'}
                    ]},
                    {type: 'section', label: 'Issues', items: [
                        {type: 'link', href: '#', content: 'AUI-111'},
                        {type: 'link', href: '#', disabled: 'true', content: 'AUI-222'},
                        {type: 'link', href: '#', hidden: 'true', content: 'AUI-333'},
                        {type: 'checkbox', href: '#', interactive: 'true', content: 'checkbox'},
                        {type: 'checkbox', href: '#', interactive: 'true', checked: 'true', content: 'checkbox checked'},
                        {type: 'radio', href: '#', interactive: 'true', content: 'radio'},
                        {type: 'radio', href: '#', interactive: 'true', checked: 'true', content: 'radio checked'}
                    ]}
                ];

                var constructed = makeFixturePointingTo('/two-sections', twoSectionResponse);

                dropdown = skate.init(constructed.dropdown);
                trigger = skate.init(constructed.trigger);
            });

            it('clicking on the trigger eventually opens a dropdown with two sections', function () {
                click(trigger);
                expect(dropdown.querySelectorAll('aui-section').length).to.equal(0);
                server.respond();
                expect(dropdown.querySelectorAll('aui-section').length).to.equal(2);
            });

            it('opening the dropdown twice does not render the information in the dropdown twice', function () {
                click(trigger);
                server.respond();

                expect(dropdown.querySelectorAll('aui-section').length).to.equal(2);

                click(trigger);
                click(trigger);

                clock.tick(2000);

                expect(dropdown.querySelectorAll('aui-section').length).to.equal(2);
            });

            it('closing the dropdown before the server responds does not open the dropdown when the server does respond', function () {
                click(trigger);

                expect(dropdown).to.be.visible;

                click(trigger);
                server.respond();

                expect(dropdown).to.not.be.visible;
            });

            it('links have correct hrefs', function () {
                click(trigger);
                server.respond();
                expect(dropdown.querySelector('aui-section aui-item-link a').getAttribute('href')).to.equal('#aui');
            });

            it('loads the dropdown when hovering, and displays instantly on click', function () {
                hover(trigger);
                server.respond();
                click(trigger);
                expect(dropdown).to.be.visible;
            });

            describe('with stubbed jQuery.ajax', function () {
                beforeEach(function () {
                    sinon.spy($, 'ajax');
                });

                afterEach(function () {
                    $.ajax.restore();
                });

                it('does not make two requests when opened, response, and opened again', function () {
                    click(trigger);
                    server.respond();
                    click(trigger);
                    click(trigger);
                    expect($.ajax.calledOnce).to.be.true;
                });

                it('does not make two requests when opened twice before responding', function () {
                    click(trigger);
                    click(trigger);
                    click(trigger);

                    server.respond();

                    expect($.ajax.calledOnce).to.be.true;
                });
            });


        });

        describe('and json response with a submenu linking to another, static section', function () {
            beforeEach(function () {
                var falseBooleanAttributeResponse = [
                    {type: 'section', label: 'Projects', items: [
                        {type: 'link', href: '#', for: 'static-submenu', content: 'Link'}
                    ]}
                ];

                var constructed = makeFixturePointingTo('/submenu-section', falseBooleanAttributeResponse);

                fixtures({
                    submenu: `
                        <aui-dropdown-menu id="static-submenu">
                            <aui-section>
                                <aui-item-link href="#submenu">Submenu</aui-item-link>
                            </aui-section>
                        </aui-dropdown-menu>
                    `
                }, false);

                dropdown = skate.init(constructed.dropdown);
                trigger = skate.init(constructed.trigger);
            });

            it('clicking the submenu trigger opens the static submenu', function (done) {
                click(trigger);
                server.respond();
                var dropdownSubmenuTrigger = dropdown.querySelector('aui-item-link');
                afterMutations(function () {
                    click(dropdownSubmenuTrigger.querySelector('a'));

                    var submenu = document.getElementById('static-submenu');
                    expect(submenu).to.be.visible;
                    done();
                });
            });
        });

        describe('and json repsonse with boolean attributes other than "true", after clicking and server response,', function () {
            beforeEach(function () {
                var falseBooleanAttributeResponse = [
                    {type: 'section', label: 'Projects', items: [
                        {type: 'link', href: '#', disabled: 'false', content: 'disabled = "false"'},
                        {type: 'link', href: '#', disabled: 'not-true', content: 'disabled = not-true'},
                        {type: 'link', href: '#', disabled: '', content: 'disabled = ""'},
                        {type: 'link', href: '#', disabled: 'true', content: 'disabled = "true"'},
                        {type: 'link', href: '#', disabled: true, content: 'disabled = true'},
                        {type: 'link', href: '#', disabled: false, content: 'disabled = true'},
                        {type: 'link', href: '#', content: 'disabled = true'},
                    ]}
                ];

                var constructed = makeFixturePointingTo('/false-boolean-attribute', falseBooleanAttributeResponse);

                dropdown = skate.init(constructed.dropdown);
                trigger = skate.init(constructed.trigger);

                click(trigger);
                server.respond();
            });

            it('disabled: "false" means the item is disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[0].hasAttribute('disabled')).to.be.true;
            });

            it('disabled: "not-true" means the item is disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[1].hasAttribute('disabled')).to.be.true;
            });

            it('disabled: "" means the item is not disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[2].hasAttribute('disabled')).to.be.false;
            });

            it('disabled: "true" means the item is disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[3].hasAttribute('disabled')).to.be.true;
            });

            it('disabled: true means the item is disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[4].hasAttribute('disabled')).to.be.true;
            });

            it('disabled: false means the item is not disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[5].hasAttribute('disabled')).to.be.false;
            });

            it('disabled unspecified means the item is not disabled', function () {
                expect(dropdown.querySelectorAll('aui-item-link')[6].hasAttribute('disabled')).to.be.false;
            });
        });
    });
});
