import '@atlassian/aui/src/js/aui/inline-dialog2';
import '@atlassian/aui/src/js/aui/trigger';
import $ from '@atlassian/aui/src/js/aui/jquery';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import { click } from '../../helpers/all';
import InlineDialog from '@atlassian/aui/src/js/aui/inline-dialog';
import skate from '@atlassian/aui/src/js/aui/internal/skate';

describe('aui/inline-dialog-integration', function () {
    describe('Inline dialog 1 (with "closeOthers: true")', function () {
        var $inlineDialog1;
        var $inlineDialog1Trigger;
        var clock;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            $inlineDialog1Trigger = $('<div class="dialog-trigger"></div>').appendTo('#test-fixture');
            $inlineDialog1 = createInlineDialog1();
        });

        afterEach(function () {
            clock.restore();
            $inlineDialog1.remove();
        });

        function createInlineDialog1 () {
            return InlineDialog($inlineDialog1Trigger, 1,
                function (content, trigger, showPopup) {
                    showPopup();
                }, {closeOthers: true}
            );
        }

        function isInlineDialog1Visible () {
            return $inlineDialog1.css('display') === 'block';
        }

        describe('and Inline dialog 2', function () {
            var inlineDialog2;
            var inlineDialog2Trigger;

            function isInlineDialog2Visible () {
                return inlineDialog2.open === true;
            }

            beforeEach(function () {
                inlineDialog2Trigger = $('<button data-aui-trigger="" aria-controls="inline-dialog2"></button>').appendTo('#test-fixture').get(0);
                inlineDialog2 = $(aui.inlineDialog2.inlineDialog2({
                    id: 'inline-dialog2',
                    alignment: 'bottom center',
                    respondsTo: 'toggle',
                    content: '<h3>I was generated by soy :)</h3>'
                })).appendTo('#test-fixture').get(0);
                skate.init(inlineDialog2Trigger);
                skate.init(inlineDialog2);
            });

            describe('after clicking on Inline Dialog 1\'s trigger', function () {
                beforeEach(function () {
                    $inlineDialog1Trigger.click();
                    clock.tick(500);
                });

                it('inline dialog 1 is visible', function () {
                    expect(isInlineDialog1Visible()).to.equal(true);
                });
            });

            describe('after clicking on Inline Dialog 2\'s trigger', function () {
                beforeEach(function () {
                    click(inlineDialog2Trigger);
                    clock.tick(500);
                });

                it('inline dialog 2 is visible', function () {
                    expect(isInlineDialog2Visible()).to.equal(true);
                });
            });
        });
    });
});
