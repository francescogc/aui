import $ from '@atlassian/aui/src/js/aui/jquery';
import '@atlassian/aui/src/js/aui/expander';
import { click } from '../../helpers/all';

describe('aui/expander', function() {
    describe('Expander Tests -', function () {
        let expander;
        let trigger;
        let clock;
        let textBlob = `
What happens when you add a shiny new browser
 to a stack of already-disagreeing citizens? You’ll inevitably find some bugs.
 This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix.
 The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and
 ready for the newest member of the browser family.
 While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments;
 plus some undesirable behaviours.
 `;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        function initialiseExpanded() {
            $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content" aria-expanded="true">${textBlob}</div>
<a id="test-expander-trigger" data-replace-text="Read more" class="aui-expander-trigger" aria-controls="test-expander">Read less</a>
`);
            expander = $('#test-expander').get(0);
            trigger = $('#test-expander-trigger').get(0);
        }

        function initialiseCollapsed() {
            $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content">${textBlob}</div>
<a id="test-expander-trigger" data-replace-text="Read less" class="aui-expander-trigger" aria-controls="test-expander">Read more</a>
`);
            expander = $('#test-expander').get(0);
            trigger = $('#test-expander-trigger').get(0);
        }

        function initialiseShortContent() {
            $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content">
    ${textBlob}
</div>
<div id="test-expander-trigger" data-replace-text="Show less" class="aui-expander-trigger" aria-controls="test-expander">
    <div class="aui-expander-short-content">My short content</div>
    <a>Show more</a>
</div>
`);
            expander = $('#test-expander').get(0);
            trigger = $('#test-expander-trigger').get(0);
        }

        function initialiseReplaceSelector() {
            $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content" aria-expanded="false">${textBlob}</div>
<a id="test-expander-trigger" data-replace-text="Show less" data-replace-selector=".test-trigger-button" class="aui-expander-trigger" aria-controls="test-expander" aria-expanded="false">
    <button class="test-trigger-button aui-button aui-button-link" aria-busy="false">Show more</button>
</a>
`);
            expander = $('#test-expander').get(0);
            trigger = $('#test-expander-trigger').get(0);
        }

        function initialiseRevealText() {
            $('#test-fixture').html(`
<div id="container">
    <div id="test-expander" class="aui-expander-content" aria-expanded="false" style="min-height: 1em">
        ${textBlob}
        <a id="test-expander-trigger" data-replace-text="Show less" class="aui-expander-trigger aui-expander-reveal-text" aria-controls="test-expander" aria-expanded="false">
            Show more
        </a>
    </div>
</div>
`);
            expander = $('#test-expander').get(0);
            trigger = $('#test-expander-trigger').get(0);
        }

        function getExpanderProperties() {
            return {
                'trigger': {
                    'rawText': $(trigger).text(),
                    'text': $(trigger).text().trim()
                },
                'expander': {
                    'height': $(expander).outerHeight(),
                    'isExpanded': expander.getAttribute('aria-expanded') === 'true',
                    'isHidden': expander.getAttribute('aria-hidden') === 'true'
                }
            };
        }

        it('Expander test: Defaults to closed initially', function () {
            initialiseCollapsed();
            const properties = getExpanderProperties();
            expect(properties.expander.isExpanded).to.be.false;
            expect(properties.expander.height).to.equal(0);
        });

        it('Expander test: Toggle changes height of expander', function () {
            initialiseCollapsed();
            const properties = getExpanderProperties();
            click(trigger);
            const newProperties = getExpanderProperties();
            expect(properties.expander.height).to.not.equal(newProperties.expander.height);
        });

        it('Expander test: Toggling twice returns to initial state', function () {
            initialiseCollapsed();
            const properties = getExpanderProperties();
            click(trigger);
            click(trigger);
            const newProperties = getExpanderProperties();
            expect(properties.height).to.equal(newProperties.height);
        });

        it('Expander test: Expanding and closing performs as expected', function () {
            initialiseCollapsed();
            let properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Read more');
            expect(properties.expander.height).to.equal(0);
            expect(properties.expander.isExpanded).to.be.false;
            click(trigger);
            properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Read less');
            expect(properties.expander.height).to.not.equal(0);
            expect(properties.expander.isExpanded).to.be.true;
            expect(properties.expander.isHidden).to.be.false;
            click(trigger);
            properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Read more');
            expect(properties.expander.height).to.equal(0);
            expect(properties.expander.isExpanded).to.be.false;
            expect(properties.expander.isHidden).to.be.true;
        });

        it('Expander test: Test initialisation as expanded', function () {
            initialiseExpanded();
            const properties = getExpanderProperties();
            expect(properties.expander.isExpanded).to.be.true;
            expect(properties.expander.height).to.be.at.least(0);
        });

        it('Expander test: Test collapsing after initialisation as expanded', function () {
            initialiseExpanded();
            click(trigger);
            const properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Read more');
            expect(properties.expander.height).to.equal(0);
            expect(properties.expander.isExpanded).to.be.false;
            expect(properties.expander.isHidden).to.be.true;
        });

        it('Expander test: Short content test displays only when collapsed', function () {
            initialiseShortContent();
            const shortContent = $('.aui-expander-short-content');
            expect(shortContent.height()).to.not.equal(0);
            expect(getExpanderProperties().expander.isExpanded).to.be.false;

            click(trigger);
            expect(shortContent.height()).to.equal(0);
            expect(getExpanderProperties().expander.isExpanded).to.be.true;
        });

        it('Expander test: Trigger replace selector modifies right element', function() {
            initialiseReplaceSelector();
            const triggerButton = trigger.querySelector('.test-trigger-button');
            expect(triggerButton.textContent).to.equal('Show more');
            click(trigger);
            expect(triggerButton.textContent).to.equal('Show less');
            click(trigger);
            expect(triggerButton.textContent).to.equal('Show more');
        });

        it('Expander test: Test expected behaviour of AUI ADG reveal text pattern', function() {
            initialiseRevealText();
            let properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Show more');
            expect($(trigger).css('position')).to.equal('absolute');
            expect(properties.expander.height).to.be.above(0);
            expect(properties.expander.isExpanded).to.be.false;
            click(trigger);
            properties = getExpanderProperties();
            expect(properties.trigger.text).to.equal('Show less');
            expect($(trigger).css('position')).to.equal('relative');
            expect(properties.expander.isExpanded).to.be.true;
        });
    });
});
