import '@atlassian/aui/src/js/aui/toggle';
import $ from '@atlassian/aui/src/js/aui/jquery';
import i18n from '@atlassian/aui/src/js/aui/i18n';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { INPUT_SUFFIX } from '@atlassian/aui/src/js/aui/internal/constants';
import {
    afterMutations,
    click,
    fixtures,
    focus,
    hover,
} from '../../helpers/all';

describe('aui/toggle', function () {
    var ENABLED = 'Enabled';
    var DISABLED = 'Disabled';

    var toggle;
    var input;

    function disableToggle() {
        toggle.disabled = true;
        // and now, because understanding the behaviour in IE11
        // with its polyfilled MutationObserver and friends is painful...
        // we will cheat a bit.
        input.disabled = true;
    }

    describe('Basic construction and initialization', function () {
        beforeEach(function () {
            var dom = fixtures({
                toggle: '<aui-toggle label="Foo"></aui-toggle>'
            });
            skate.init(dom.toggle);

            toggle = dom.toggle;
            input = dom.toggle._input;
        });

        it('creates a input element', function () {
            expect(input).to.exist;
            expect(input.type).to.equal('checkbox');
        });

        it('tooltip-on and tooltip-off are initialized', function () {
            expect(toggle.tooltipOn).to.equal(i18n.getText('aui.toggle.on'));
            expect(toggle.tooltipOff).to.equal(i18n.getText('aui.toggle.off'));
        });

        it('fires no events during its construction', function (done) {
            const eventList = 'change click input focus blur';
            let eventsFired = [];
            let handler = e => eventsFired.push(e);
            $('#test-fixture').on(eventList, handler);
            $('#test-fixture').html('<aui-toggle label="Unchecked by default" id="unchecked-default"></aui-toggle>');
            $('#test-fixture').html('<aui-toggle label="Checked by default" id="checked-default" checked></aui-toggle>');
            afterMutations(() => {
                $('#test-fixture').off(eventList, handler);
                expect(eventsFired).to.be.empty;
                done();
            }, 50);
        });
    });

    describe('Attribute & properties - ', function () {
        describe('Basic behaviors -', function () {
            var form;

            beforeEach(function () {
                var dom = fixtures({
                    toggle: `<aui-toggle id="my-toggle" checked disabled
                            form="my-form" name="toggle-name" value="toggle-value"
                            tooltip-on="${ENABLED}" tooltip-off="${DISABLED}" label="My label"></aui-toggle>`,
                    form: '<form id="my-form"></form>'
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
                input = dom.toggle._input;
                form = dom.form;
            });

            it('Attributes are copied over input', function () {
                expect(input.id).to.equal('my-toggle' + INPUT_SUFFIX);
                expect(input.checked).to.be.true;
                expect(input.disabled).to.be.true;
                expect(input.getAttribute('form')).to.equal('my-form');
                expect(input.name).to.equal('toggle-name');
                expect(input.value).to.equal('toggle-value');
                expect(input.getAttribute('aria-label')).to.equal('My label');
            });

            it('All attributes are accessible via properties', function () {
                expect(toggle.id).to.equal('my-toggle');
                expect(toggle.checked).to.be.true;
                expect(toggle.disabled).to.be.true;
                expect(toggle.form).to.equal(form);
                expect(toggle.name).to.equal('toggle-name');
                expect(toggle.value).to.equal('toggle-value');
                expect(toggle.busy).to.be.false;
                expect(toggle.tooltipOn).to.equal(ENABLED);
                expect(toggle.tooltipOff).to.equal(DISABLED);
                expect(toggle.label).to.equal('My label');
            });

            describe('Remove toggle attributes', function () {
                beforeEach(function (done) {
                    // AUI-4935 - must remove 'disabled' first
                    toggle.removeAttribute('disabled');
                    toggle.removeAttribute('id');
                    toggle.removeAttribute('checked');
                    toggle.removeAttribute('form');
                    toggle.removeAttribute('name');
                    toggle.removeAttribute('value');
                    toggle.removeAttribute('tooltip-on');
                    toggle.removeAttribute('tooltip-off');
                    toggle.removeAttribute('label');
                    afterMutations(done);
                });

                it('remove attributes on the input (or set to default)', function () {
                    expect(input.id).to.be.empty;
                    expect(input.checked).to.be.false;
                    expect(input.disabled).to.be.false;
                    expect(input.form).to.be.null;
                    expect(input.name).to.equal('');
                    expect(input.value).to.equal('on');
                    expect(input.getAttribute('tooltip-on')).to.equal(i18n.getText('aui.toggle.on'));
                    expect(input.getAttribute('tooltip-off')).to.equal(i18n.getText('aui.toggle.off'));
                    expect(input.hasAttribute('aria-label')).to.be.false;
                });

                it('remove property values (or set to default)', function () {
                    expect(toggle.checked).to.be.false;
                    expect(toggle.disabled).to.be.false;
                    expect(toggle.form).to.be.null;
                    expect(toggle.name).to.equal('');
                    expect(toggle.value).to.equal('on');
                    expect(toggle.tooltipOn).to.be.null;
                    expect(toggle.tooltipOff).to.be.null;
                    expect(toggle.label).to.be.null;
                });
            });

            describe('Change toggle properties -', function () {
                beforeEach(function (done) {
                    // AUI-4935 - must remove 'disabled' first
                    toggle.disabled = false;
                    toggle.id = 'new-id';
                    toggle.checked = false;
                    toggle.name = 'new-name';
                    toggle.value = 'new-value';
                    toggle.busy = true;
                    toggle.tooltipOn = 'new-tooltip-on';
                    toggle.tooltipOff = 'new-tooltip-off';
                    toggle.label = 'new-label';
                    afterMutations(done);
                });

                it('make sure property values are actually changed', function () {
                    expect(toggle.id).to.equal('new-id');
                    expect(toggle.checked).to.be.false;
                    expect(toggle.disabled).to.be.false;
                    expect(toggle.name).to.equal('new-name');
                    expect(toggle.value).to.equal('new-value');
                    expect(toggle.busy).to.be.true;
                    expect(toggle.tooltipOn).to.equal('new-tooltip-on');
                    expect(toggle.tooltipOff).to.equal('new-tooltip-off');
                    expect(toggle.label).to.equal('new-label');
                });

                it('change toggle attributes', function () {
                    expect(toggle.getAttribute('id')).to.equal('new-id');
                    expect(toggle.hasAttribute('checked')).to.be.false;
                    expect(toggle.hasAttribute('disabled')).to.be.false;
                    expect(toggle.getAttribute('name')).to.equal('new-name');
                    expect(toggle.getAttribute('value')).to.equal('new-value');
                    expect(toggle.getAttribute('tooltip-on')).to.equal('new-tooltip-on');
                    expect(toggle.getAttribute('tooltip-off')).to.equal('new-tooltip-off');
                    expect(toggle.getAttribute('label')).to.equal('new-label');
                });

                it('change input properties or attributes', function () {
                    expect(input.id).to.equal('new-id' + INPUT_SUFFIX);
                    expect(input.checked).to.equal(false, 'checked');
                    expect(input.disabled).to.equal(false, 'disabled');
                    expect(input.name).to.equal('new-name');
                    expect(input.value).to.equal('new-value');
                    expect(input.getAttribute('tooltip-on')).to.equal('new-tooltip-on');
                    expect(input.getAttribute('tooltip-off')).to.equal('new-tooltip-off');
                    expect(input.getAttribute('aria-busy')).to.equal('true');
                    expect(input.getAttribute('aria-label')).to.equal('new-label');
                });
            });
        });

        describe('checked -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: '<aui-toggle id="my-toggle" ' +
                        'checked name="toggle-name" value="toggle-value" ' +
                        'label="My label"></aui-toggle>',
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            it('Checked state still in sync after the toggle button is clicked', function (done) {
                expect(toggle.checked).to.be.true;
                afterMutations(function () {
                    toggle.click();
                    afterMutations(function () {
                        expect(toggle.checked).to.be.false;
                        toggle.checked = true;
                        expect(toggle.checked).to.be.true;
                        done();
                    }, 50);
                }, 50);
            });

            it('Checked state stays in sync after setting checked property and then clicking', function (done) {
                toggle.checked = false;
                afterMutations(function () {
                    toggle.click();
                    afterMutations(function () {
                        expect(toggle.checked).to.be.true;
                        expect(toggle.hasAttribute('checked')).to.be.true;
                        done();
                    }, 50);
                }, 50);
            });

            it('Checked state stays in sync after setting checked property to false and then setting the attribute directly', function (done) {
                toggle.checked = false;

                afterMutations(function () {
                    toggle.setAttribute('checked', '');

                    afterMutations(function () {
                        expect(toggle.checked).to.be.true;
                        expect(toggle.hasAttribute('checked')).to.be.true;
                        done();
                    });
                });
            });

            it('Checked state stays in sync after setting checked property to true and then removing the attribute directly', function (done) {
                toggle.checked = false;
                toggle.checked = true;

                afterMutations(function () {
                    toggle.removeAttribute('checked');

                    afterMutations(function () {
                        expect(toggle.checked).to.be.false;
                        expect(toggle.hasAttribute('checked')).to.be.false;
                        done();
                    });
                });
            });
        });

        describe('value -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: '<aui-toggle id="my-toggle" ' +
                        'checked name="toggle-name" value="toggle-value" ' +
                        'label="My label"></aui-toggle>',
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            it('Assign null to value will set the value to empty string', function (done) {
                toggle.value = null;
                afterMutations(function () {
                    expect(toggle.value).to.equal('');
                    done();
                });
            });
        });
    });

    describe('Tooltips -', function () {
        var clock;

        function getTooltip () {
            return document.querySelector('[role="tooltip"]');
        }

        function removeAllTooltips() {
            $('[role="tooltip"]').remove();
        }

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
            removeAllTooltips()
        });

        describe('Initialization -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: '<aui-toggle label="Foo"></aui-toggle>'
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
                input = toggle._input;
            });

            it('show default text if tooltip-off is not set', function () {
                hover(input);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip.textContent).to.equal(i18n.getText('aui.toggle.off'));
            });

            it('show default text if tooltip-on is not set', function () {
                toggle.checked = true;
                hover(input);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip.textContent).to.equal(i18n.getText('aui.toggle.on'));
            });
        });

        describe('Behaviors -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: `<aui-toggle label="Foo" tooltip-on="${ENABLED}" tooltip-off="${DISABLED}"/>`
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
                input = toggle._input;
            });

            it(`toggle shows a tooltip with '${DISABLED}' text in off state`, function () {
                hover(input);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(DISABLED);
            });

            it(`toggle shows a tooltip with '${ENABLED}' text in off state`, function () {
                toggle.checked = true;
                hover(input);
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(ENABLED);
            });

            it('shown on disabled button', function () {
                toggle.disabled = true;
                hover(toggle);
                hover(input); // this is for you IE :(
                clock.tick(500);
                var tooltip = getTooltip();
                expect(tooltip).to.exist;
                expect(tooltip.textContent).to.equal(DISABLED);
            });
        });
    });

    describe('Behaviors -', function () {

        beforeEach(function () {
            var dom = fixtures({
                toggle: '<aui-toggle id="my-toggle" label="Foo"></aui-toggle>'
            });
            skate.init(dom.toggle);

            toggle = dom.toggle;
            input = dom.toggle._input;
        });

        describe('Normal toggle -', function () {
            it('toggle is focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(1);
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(1);
            });

            it('click can toggle', function () {
                click(toggle);
                expect(toggle.checked).to.be.true;
            });

            it('focus and press space to toggle', function () {
                focus(toggle);
                expect(document.activeElement.id).to.be.equal('my-toggle' + INPUT_SUFFIX);
                // cannot simulate SPACE, so use click event on active element instead
                document.activeElement.click();
                expect(toggle.checked).to.be.true;
            });
        });

        describe('Disabled toggle -', function () {
            beforeEach(function (done) {
                disableToggle();
                afterMutations(done);
            });

            it('is not focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(0, 'an element was focusable');
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(0, 'an element was in the tab order');
            });

            it('click will not toggle', function () {
                click(toggle);
                expect(toggle.checked).to.be.false;
            });

            it('javascript - .focus() is not possible', function () {
                focus(toggle);
                expect(document.activeElement).to.not.equal(input);
            });
        });

        describe('Checked toggle -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: '<aui-toggle id="my-toggle" checked label="Foo"></aui-toggle>'
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            describe('Programatically toggle -', function () {
                let onChange;

                beforeEach(function () {
                    onChange = sinon.spy();
                    toggle.addEventListener('change', onChange);
                });

                it('should not fire "change" event, on property "checked" changed to "true"', function () {
                    toggle.checked = true;
                    onChange.should.not.have.been.called;
                });

                it('should fire "change" event, on property "checked" changed to "false"', function (done) {
                    toggle.checked = false;
                    afterMutations(function () {
                        onChange.should.have.been.calledOnce;
                        done();
                    });

                });

                it('should fire "change" event, on attribute "checked" removed', function (done) {
                    toggle.removeAttribute('checked');
                    afterMutations(function () {
                        onChange.should.have.been.calledOnce;
                        done();
                    });
                });

                it('should not fire "change" event, on attribute "checked" added', function () {
                    toggle.setAttribute('checked', '');
                    onChange.should.not.have.been.called;
                });
            });

            describe('defaultChecked property - ', () => {
                let input;
                beforeEach(() => {
                    input = toggle.querySelector('input[type=checkbox]');
                });

                it('should not change defaultChecked input property on click', () => {
                    toggle.click();
                    expect(input.defaultChecked).to.be.true;
                });

                it('should not change defaultChecked input property on attribute removed', () => {
                    toggle.removeAttribute('checked');
                    expect(input.defaultChecked).to.be.true;
                });
            });
        });

        describe('Unchecked toggle -', function () {
            beforeEach(function () {
                var dom = fixtures({
                    toggle: '<aui-toggle id="my-toggle" label="Foo"></aui-toggle>'
                });
                skate.init(dom.toggle);

                toggle = dom.toggle;
            });

            describe('Programatically toggle -', function () {
                let onChange;

                beforeEach(function () {
                    onChange = sinon.spy();
                    toggle.addEventListener('change', onChange);
                });

                it('should fire "change" event, on property "checked" changed to "true"', function (done) {
                    toggle.checked = true;
                    afterMutations(function () {
                        onChange.should.have.been.calledOnce;
                        done();
                    });
                });

                it('should not fire "change" event, on property "checked" changed to "false"', function () {
                    toggle.checked = false;
                    onChange.should.not.have.been.called;
                });

                it('should not fire "change" event, on attribute "checked" removed', function () {
                    toggle.removeAttribute('checked');
                    onChange.should.not.have.been.called;
                });

                it('should fire "change" event, on attribute "checked" added', function (done) {
                    toggle.setAttribute('checked', '');
                    afterMutations(function () {
                        onChange.should.have.been.calledOnce;
                        done();
                    });
                });
            });

            describe('defaultChecked property - ', () => {
                let input;
                beforeEach(() => {
                    input = toggle.querySelector('input[type=checkbox]');
                });

                it('should not change defaultChecked input property on click', () => {
                    toggle.click();
                    expect(input.defaultChecked).to.be.false;
                });

                it('should not change defaultChecked input property on attribute added', () => {
                    toggle.setAttribute('checked', '');
                    expect(input.defaultChecked).to.be.false;
                });
            });
        });

        describe('Busy state -', function () {
            function getSpinner() {
                return document.querySelector('aui-spinner');
            }

            beforeEach(function () {
                toggle.busy = true;
            });

            it('spinner is shown', function () {
                expect(getSpinner()).to.exist;
            });

            it('sets indeterminate on the input', function () {
                expect(input.indeterminate).to.equal(true);
                toggle.busy = false;
                expect(input.indeterminate).to.equal(false);
            });

            it('adds .indeterminate-checked on the input', function () {
                input.checked = true;
                expect($(input).is('.indeterminate-checked')).to.equal(false, 'pre-busy');
                toggle.busy = true;
                expect($(input).is('.indeterminate-checked')).to.equal(true, 'busy');
                toggle.busy = false;
                expect($(input).is('.indeterminate-checked')).to.equal(false, 'post-busy');
            });

            it('sets aria-busy on the input', function () {
                expect(input.getAttribute('aria-busy')).to.equal('true');
                toggle.busy = false;
                var ariaBusyValues = [null, 'false'];
                expect(ariaBusyValues.indexOf(input.getAttribute('aria-busy'))).to.not.equal(-1);
            });

            it('is focusable and tabbable', function () {
                var $items = $('#test-fixture :aui-focusable');
                expect($items.length).to.equal(1);
                $items = $('#test-fixture :aui-tabbable');
                expect($items.length).to.equal(1);
            });

            it('is not clickable', function () {
                click(toggle);
                expect(toggle.checked).to.be.false;
            });
        });
    });
});
