import $ from '@atlassian/aui/src/js/aui/jquery';
import navigation from '@atlassian/aui/src/js/aui/navigation';
import skate from '@atlassian/aui/src/js/aui/internal/skate';

describe('aui/navigation', function () {
    var $fixture;

    beforeEach(function () {
        $fixture = $('#test-fixture');
        $fixture.html(`
            <ul class="aui-nav">
                <li aria-expanded="false">
                    <a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>
                    <a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>
                    <ul class="aui-nav" title="one" data-more="" data-more-limit="3">
                        <li class="aui-nav-selected"><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="aui-nav">
                <li aria-expanded="false">
                    <a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>
                    <a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>
                    <ul class="aui-nav" title="one" data-more="" data-more-limit="3">
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                    </ul>
                </li>
            </ul>
        `);
        skate.init($fixture);
    });

    afterEach(function () {
        $fixture.remove();
    });

    it('globals', function () {
        expect(AJS.navigation.toString()).to.equal(navigation.toString());
    });

    // Expander tests
    it('Expander', function () {
        var $toggle = $fixture.find('>.aui-nav:first .aui-nav-subtree-toggle');
        var $expander = $fixture.find('>.aui-nav:first li[aria-expanded]');

        expect($expander.attr('aria-expanded')).to.equal('true');
        $toggle.click();
        expect($expander.attr('aria-expanded')).to.equal('false');
        $toggle.click();
        expect($expander.attr('aria-expanded')).to.equal('true');
    });

    // Make sure aui-nav-child-selected class is added
    it('Child Selected class', function () {
        expect($fixture.find('.aui-nav-selected').closest('li:not(.aui-nav-selected)').hasClass('aui-nav-child-selected')).to.be.true;
    });

    // 'More...' tests
    it('More Initialisation', function () {
        expect($fixture.find('>.aui-nav:last .aui-nav-more').length).to.be.defined;
    });

    it('No More Initialisation if selected', function () {
        expect($fixture.find('>.aui-nav:first .aui-nav-more').length).to.not.be.defined;
    });

    it('More Expansion', function () {
        $fixture.find('>.aui-nav:last .aui-nav-more a').click();

        expect($fixture.find('>.aui-nav:last .aui-nav-more').length).to.not.be.defined;
    });

    it('Show \'More\' again when re-expanded', function () {
        var $toggle = $fixture.find('>.aui-nav:last .aui-nav-subtree-toggle');

        $fixture.find('>.aui-nav:last .aui-nav-more a').click();

        expect($fixture.find('>.aui-nav:last .aui-nav-more').length).to.not.be.defined;

        $toggle.click();
        $toggle.click();

        expect($fixture.find('.aui-nav-more').length).to.be.defined;
    });
});
