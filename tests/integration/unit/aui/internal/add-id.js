import addId from '@atlassian/aui/src/js/aui/internal/add-id';

describe('aui/internal/add-id', function () {
    it('globals', function () {
        expect(AJS._addID.toString()).to.equal(addId.toString());
    });
});
