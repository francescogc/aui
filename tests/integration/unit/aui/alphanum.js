import alphanum from '@atlassian/aui/src/js/aui/alphanum';

describe('aui/alphanum', function () {
    it('globals', function () {
        expect(AJS.alphanum.toString()).to.equal(alphanum.toString());
    });

    it('API', function () {
        function assertAlphaNum(a, b, expected) {
            let actual = alphanum(a, b);
            expect(actual).to.equal(expected);

            // try in reverse
            actual = alphanum(b, a);
            expect(actual).to.equal(expected * -1);
        }

        assertAlphaNum('a', 'a', 0);
        assertAlphaNum('a', 'b', -1);
        assertAlphaNum('b', 'a', 1);

        assertAlphaNum('a0', 'a1', -1);
        assertAlphaNum('a10', 'a1', 1);
        assertAlphaNum('a2', 'a1', 1);
        assertAlphaNum('a2', 'a10', -1);
    });
});
