import debounce, { debounceImmediate } from '@atlassian/aui/src/js/aui/debounce';

describe('aui/debounce', function () {
    it('globals', function () {
        expect(AJS.debounce.toString()).to.equal(debounce.toString());
        expect(AJS.debounceImmediate.toString()).to.equal(debounceImmediate.toString());
    });
});
