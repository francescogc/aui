import clone from '@atlassian/aui/src/js/aui/clone';

describe('aui/clone', function () {
    it('globals', function () {
        expect(AJS.clone.toString()).to.equal(clone.toString());
    });
});
