import $ from '@atlassian/aui/src/js/aui/jquery';
import RestfulTable from '@atlassian/aui/src/js/aui/restful-table';

// For Checking global existence.
import RestfulTableClassNames from '@atlassian/aui/src/js/aui/restful-table/class-names';
import RestfulTableCustomCreateView from '@atlassian/aui/src/js/aui/restful-table/custom-create-view';
import RestfulTableCustomEditView from '@atlassian/aui/src/js/aui/restful-table/custom-edit-view';
import RestfulTableCustomReadView from '@atlassian/aui/src/js/aui/restful-table/custom-read-view';
import RestfulTableDataKeys from '@atlassian/aui/src/js/aui/restful-table/data-keys';
import RestfulTableEditRow from '@atlassian/aui/src/js/aui/restful-table/edit-row';
import RestfulTableEntryModel from '@atlassian/aui/src/js/aui/restful-table/entry-model';
import RestfulTableEvents from '@atlassian/aui/src/js/aui/restful-table/event-names';
import RestfulTableRow from '@atlassian/aui/src/js/aui/restful-table/row';

describe('aui/restful-table', function () {
    it('globals', function () {
        expect(AJS.RestfulTable.toString()).to.equal(RestfulTable.toString());

        expect(AJS.RestfulTable.ClassNames.toString()).to.equal(RestfulTableClassNames.toString());
        expect(AJS.RestfulTable.CustomCreateView.toString()).to.equal(RestfulTableCustomCreateView.toString());
        expect(AJS.RestfulTable.CustomEditView.toString()).to.equal(RestfulTableCustomEditView.toString());
        expect(AJS.RestfulTable.CustomReadView.toString()).to.equal(RestfulTableCustomReadView.toString());
        expect(AJS.RestfulTable.DataKeys.toString()).to.equal(RestfulTableDataKeys.toString());
        expect(AJS.RestfulTable.EditRow.toString()).to.equal(RestfulTableEditRow.toString());
        expect(AJS.RestfulTable.EntryModel.toString()).to.equal(RestfulTableEntryModel.toString());
        expect(AJS.RestfulTable.Events.toString()).to.equal(RestfulTableEvents.toString());
        expect(AJS.RestfulTable.Row.toString()).to.equal(RestfulTableRow.toString());
    });

    describe('Initialization', function () {
        var server;
        var rt;
        var rtRows;
        var $table;

        beforeEach(function () {
            server = sinon.fakeServer.create();
            var users = [{'id': 1, 'name': 'adam'},{'id': 2, 'name': 'betty'},{'id': 3, 'name': 'chris'}];

            server.respondWith('GET', '/all', [200, {'Content-Type': 'application/json'}, JSON.stringify(users)]);

            rt = new RestfulTable({
                el: $('<table id="test-table" class="aui"></table>'),
                resources: {
                    all: '/all',
                    self: '/single'
                },
                columns: [
                    {
                        id: 'name',
                        header: 'Name'
                    }
                ]
            });

            rtRows = [];
            rt.bind(RestfulTable.Events.ROW_INITIALIZED, function (row) {
                rtRows.push(row);
            });

            $table = rt.getTable();
            $table.appendTo('#test-fixture');

            server.respond();
        });

        afterEach(function () {
            server.restore();
        });

        it('throws error when the deleteConfirmationCallback is provided but it is not a function', function () {
            expect(
                () => new RestfulTable({
                    resources: {},
                    el: $('<table id="test-table" class="aui"></table>'),
                    columns: [{id: 'name', header: 'Name'}],
                    deleteConfirmationCallback: {}
                })
            ).to.throw('RestfulTable: Init failed! deleteConfirmationCallback is not a function')
        });

        it('renders properly', function () {
            expect($table.length).to.equal(1);
            expect(rtRows.length).to.equal(3);

            expect($table.find('thead th:first').text()).to.equal('Name');
            expect($table.find('tbody.aui-restfultable-create tr').length).to.equal(1);
            expect($table.find('tbody:not(.aui-restfultable-create) tr').length).to.equal(3);
        });

        it('test - proper row data is used', function () {
            expect(rtRows[0].model.get('id')).to.equal(1);
            expect(rtRows[0].model.get('name')).to.equal('adam');

            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(0)').data('id')).to.equal(1);
            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(0)').data('name')).to.equal('adam');
            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(1) td:first').text()).to.equal('betty');
        });

        it('test - edit works', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');

            edited.$('input[name=name]').val('edited');
            edited.submit(false);

            server.respondWith('PUT', '/single/1', function (xhr) {
                xhr.respond(200, {'Content-Type': 'application/json'}, xhr.requestBody);
            });
            server.respond();

            var editResponse = JSON.parse(server.requests[1].requestBody);
            expect(editResponse.name).to.equal('edited');
        });

        it('test - fieldFocusSelector is defined for create row', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');
            edited.$('input[name=name]').focus();
            var createRow = rt.getCreateRow();
            createRow.focus('name');

            expect(createRow.$el.get(0).firstChild.firstChild).to.equal(document.activeElement);
        });
    });

    describe('Custom object serialization', function () {
        var server;
        var rt;
        var rtRows;
        var $table;

        beforeEach(function () {
            server = sinon.fakeServer.create();
            var users = [{'id': 1, 'name': 'adam'}];

            server.respondWith('GET', '/all', [200, {'Content-Type': 'application/json'}, JSON.stringify(users)]);
            rt = new RestfulTable({
                el: $('<table id="test-table" class="aui"></table>'),
                resources: {
                    all: '/all',
                    self: '/single'
                },
                columns: [
                    {
                        id: 'name',
                        header: 'Name'
                    }
                ],
                views: {
                    editRow: RestfulTable.EditRow.extend({
                        initialize: function () {
                            RestfulTable.EditRow.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
                        },
                        serializeObject: function () {
                            return {
                                name: this.$el.find(':input[name=name]').val() + ' serialized'
                            };
                        }
                    })
                }
            });

            rtRows = [];
            rt.bind(RestfulTable.Events.ROW_INITIALIZED, function (row) {
                rtRows.push(row);
            });

            $table = rt.getTable();
            $table.appendTo('#test-fixture');
            server.respond();
        });

        afterEach(function () {
            server.restore();
        });

        it('should serialize updated table input on edit', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');
            edited.$('input[name=name]').val('edited');
            edited.submit(false);
            server.respondWith('PUT', '/single/1', function (xhr) {
                JSON.parse(xhr.requestBody);
                xhr.respond(200, {'Content-Type': 'application/json'}, xhr.requestBody);
            });
            server.respond();

            var editResponse = JSON.parse(server.requests[1].requestBody);
            expect(editResponse.name).to.equal('edited serialized');
        });
    });
});
