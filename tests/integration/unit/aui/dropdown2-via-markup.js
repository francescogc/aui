import '@atlassian/aui/src/js/aui/dropdown2';
import $ from '@atlassian/aui/src/js/aui/jquery';
import AccessibleDropdown from './dropdown2/dropdown2-test-accessible-helper';
import { runDropdown2BehaviouralTests } from './dropdown2/behavioural-tests';
import skate from '@atlassian/aui/src/js/aui/internal/skate';

describe('Dropdown - accessible markup pattern', function () {
    function isVisible (element) {
        var $element = $(element);
        return $element.is(':visible');
    }

    describe('globals', function () {
        expect(AJS.dropdown2).to.equal(undefined);
    });

    // Dropdown2 - Construction
    // ------------------------
    //
    // Test the construction of dropdowns.
    // Checking basic state, and looking for race conditions or order of operations problems.
    //
    describe('Construction -', function () {
        var $dropdown;

        beforeEach(function () {
            $dropdown = $('<div id="dd22" class="aui-dropdown2"></div>');
        });

        it('via aui-dropdown2 class', function () {
            skate.init($dropdown);
            expect($dropdown.attr('resolved')).to.equal('');
        });

        it('hides dropdown upon construction', function () {
            skate.init($dropdown);
            expect(isVisible($dropdown)).to.be.false;
            expect($dropdown.attr('aria-hidden')).to.equal('true');
        });
    });

    describe('lazy construction', function () {
        var $trigger;
        var $dropdown;

        function click($el) {
            $el.trigger('mousedown');
            $el.click();
            $el.trigger('mouseup');
        }

        beforeEach(function () {
            sinon.useFakeTimers();
            $trigger = $('<a></a>');
            $dropdown = $('<div id="lazy-dropdown" class="aui-dropdown2"></div>');

            $('#test-fixture').append($trigger);
            $('#test-fixture').append($dropdown);
            skate.init($dropdown); //only skate.init $dropdown and not $trigger as we're testing the synchronous initialisation of the trigger

        });

        afterEach(function () {
            sinon.restore();
        });

        it('trigger is not exapndable before attributes are added', function () {
            click($trigger);
            expect($trigger.attr('aria-expanded')).to.not.equal('true');
        });

        it('dropdown is not visible before attributes are added', function () {
            click($trigger);
            expect($dropdown.is(':visible')).to.be.false;
        });

        describe('after attributes are lazily added', function () {
            beforeEach(function () {
                $trigger.addClass('aui-dropdown2-trigger');
                $trigger.attr('aria-owns', 'lazy-dropdown');
                $trigger.attr('data-aui-trigger', 'toggle');
            });

            describe('followed by clicking on the trigger', function () {
                beforeEach(function () {
                    click($trigger);
                });

                it('expands the trigger', function () {
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('makes the dropdown visible', function () {
                    expect($dropdown.is(':visible')).to.be.true;
                });

                it('puts functions on the trigger element', function () {
                    expect($trigger[0].isEnabled).to.be.a('function');
                });
            });
        });
    });

    runDropdown2BehaviouralTests(AccessibleDropdown);
});
