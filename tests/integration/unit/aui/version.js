import version from '@atlassian/aui/src/js/aui/version';

describe('aui/version', function () {
    it('globals', function () {
        expect(AJS.version.toString()).to.equal(version.toString());
    });

    it('should be a string', function () {
        expect(version).to.be.a('string');
    });

    it('should have correct version format', function () {
        expect(version).to.match(/^\d+\.\d+\.\d+(-.*)?/);
    });
});
