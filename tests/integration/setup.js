import $ from '@atlassian/aui/src/js/aui/jquery';

/* global mocha, chai, sinon, __webpack_require__ */

__webpack_require__.d = function(exports, name, getter) {
    if (!__webpack_require__.o(exports, name)) {
        let localMockValue;
        Object.defineProperty(exports, name, {
            configurable: true,
            enumerable: true,
            get: () => localMockValue || getter(),
            set: function (val) {
                localMockValue = val;
            }
        });
    }
};

mocha.setup('bdd');

window.assert = chai.assert;
window.expect = chai.expect;

chai.should();

// Chai extensions
// ---------------
chai.use(function (chai, utils) {
    utils.addProperty(chai.Assertion.prototype, 'visible', function () {
        var $el = $(this._obj);
        this.assert(
            $el.is(':visible') === true,
            'expected "#{this}" to be visible',
            'expected "#{this}" to be hidden'
        );
    });
});
