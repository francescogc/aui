const gulp = require('gulp');
const path = require('path');

const internalPackages = [
    require.resolve('@atlassian/aui/package.json'),
    require.resolve('@atlassian/aui-soy/package.json'),
];

function buildInternalDeps (watching) {
    const proc = require('child_process');

    return internalPackages.map(pkgpath => {
        const buildDepViaScript = (script) => (done) => {
            // now we chop up that script, assuming the first part is a command-line utility
            // and subsequent parts are arguments to that and only that command.
            // this assumption will break quite readily (e.g., add some && or piping in).
            const [cmd, ...args] = script.split(' ');
            const thread = proc.spawn(cmd, args, { cwd: path.dirname(pkgpath) });
            thread.stdout.on('data', data => console.log(String(data)));
            thread.stderr.on('data', data => console.error(String(data)));
            thread.on('close', done);
            thread.on('error', done);
        };

        // here, we're making the assumption that each
        // package.json file has both a 'build' and 'watch' script.
        const pkg = require(pkgpath);
        const buildIt = buildDepViaScript(pkg.scripts.build);
        const watchIt = buildDepViaScript(pkg.scripts.watch);

        if (watching) {
            const task = (done) => buildIt(function () {
                done();
                watchIt(done);
            });
            task.displayName = `<running "${pkg.scripts.watch}" in ${pkgpath}>`;
            return task;
        } else {
            const task = buildIt;
            task.displayName = `<running "${pkg.scripts.build}" in ${pkgpath}>`;
            return task;
        }
    });
}

function resolveInternalDeps () {
    const deps = [].concat(internalPackages);
    return deps.map(d => {
        const pkg = require(d);
        return path.join(path.dirname(d), path.dirname(pkg.browser), '**')
    });
}

function resolveExternalDeps () {
    const deps = [
        'requirejs/require.js',
        'sinon/pkg/sinon.js',
        'jquery/dist/jquery.js',
    ];
    return deps.map(d => require.resolve(d));
}

function resolveStaticAssets () {
    const deps = [
        path.resolve(__dirname, '../src/static/common'),
        path.resolve(__dirname, '../../test-pages/common')
    ];
    return deps.map(pathname => [path.join(pathname, '**'), '!*.soy'])
}

function resolvePageAssets () {
    const deps = [
        path.resolve(__dirname, '../../test-pages/pages')
    ];
    return deps.map(pathname => [path.join(pathname, '**'), '!*.soy'])
}

module.exports = (buildOpts) => {
    const { outDir, watch } = buildOpts;

    // this is a utility function to generate the appropriate gulp pipeline fragment
    // for "copy-pasting" files from one location to another. its implementation
    // changes depending on whether we are in a "watch" mode or not, since in watch mode
    // we need to run the task once initially, then again on subsequent changes.
    const copyToDest = (out) => asset => {
        // this is the actual "copy-paste" task.
        const task = () => gulp.src(asset).pipe(gulp.dest(out));
        task.displayName = `<copying '${String(asset)}'>`;

        // this task wraps the worker, repeating it as necessary in watch mode.
        const worker = (done) => {
            if (watch) {
                gulp.watch(asset, task);
            }
            task();
            done();
        };
        worker.displayName = `<${watch ? 'watching' : 'building'} ${String(asset)}>`;
        return worker;
    };

    // construct the pipeline.
    return gulp.parallel(
        gulp.series(
            gulp.parallel(buildInternalDeps(watch)),
            ...resolveInternalDeps().map(copyToDest(path.join(outDir, 'common')))
        ),
        ...resolveExternalDeps().map(copyToDest(path.join(outDir, 'common'))),
        ...resolveStaticAssets().map(copyToDest(path.join(outDir, 'common'))),
        ...resolvePageAssets().map(copyToDest(outDir))
    )
};
