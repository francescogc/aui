/* eslint-env node */
/* global casper */
var phantomcss = require('phantomcss');

function effectivelyDisabledAnimations() {
    var style = document.createElement('style');
    style.innerHTML = '* { ' +
        '-webkit-animation-delay: 0s !important;' +
        'animation-delay: 0s !important;' +
        '-webkit-animation-duration: 0s !important;' +
        'animation-duration: 0s !important;' +
        '-webkit-transition-duration: 0s !important;' +
        'transition-duration: 0s !important;' +
        '}';
    document.body.appendChild(style);
}

// Static elements
require('../visual-regression.js')(casper, phantomcss, 'docs').run(
    'http://localhost:8000/', // TODO: sync this with docs.opts.js
    {
        'index.html': ['body'],
        'docs/typography.html': function(casper, test, phantomcss) {
            casper.then(function() {
                phantomcss.screenshot({
                    'docs_component-nav': { selector: '.aui-page-panel-nav' },
                    'docs_typography': { selector: 'aui-page-panel-content' }
                });
            });
        },
        'docs/app-header.html': ['.aui-flatpack-example'],
        'docs/banner-example-iframe.html': ['.aui-flatpack-example'],

        'docs/forms.html': ['.aui-flatpack-example'],
        'docs/icons.html': ['.aui-flatpack-example'],
        'docs/labels.html': ['.aui-flatpack-example'],
        'docs/lozenges.html': ['.aui-flatpack-example'],
        'docs/messages.html': ['.aui-flatpack-example'],
        'docs/navigation.html': [
            '#horizontal-nav-example',
            '#vertical-nav-example',
            '#pagination-example',
            '#actions-list-example',
        ],
        'docs/page-header.html': ['#pageheader-example'],
        'docs/progress-tracker.html': [
            '#default-progress-tracker-example',
            '#inverted-progress-tracker-example'
        ],
        'docs/tabs.html': ['.aui-flatpack-example'],
        'docs/toggle-button.html': ['aui-docs-example'],
        'docs/toolbar2.html': ['.aui-flatpack-example'],
    }
);

// Interactive elements
require('../visual-regression.js')(casper, phantomcss, 'components').run(
    'http://localhost:8000/', // TODO: sync this with docs.opts.js
    {
        'docs/dialog2.html': function(casper, test, phantomcss) {
            casper.waitUntilVisible('#static-dialog-example .aui-dialog2');
            casper.then(function() {
                phantomcss.screenshot('#static-dialog-example .aui-live-demo', 'dialog2');
            });
        },
        'docs/dropdown.html': function(casper, test, phantomcss) {
            casper.then(function() {
                // If I don't do this, none of the interactions actually work :(
                phantomcss.screenshot('#logo', 'stupid_but_have_to');
            });
            casper.then(function() {
                // this is inside #submenu-example
                this.mouse.click('.aui-dropdown2-trigger[aria-controls=has-submenu]');
                this.waitUntilVisible('#has-submenu');
                this.mouse.move('#has-submenu .aui-dropdown2-sub-trigger');
                phantomcss.screenshot('#submenu-example', 'interactive_dropdown__submenus');
            });
            casper.then(function() {
                // this is inside #simple-example
                this.mouse.click('.aui-dropdown2-trigger[aria-controls=example-dropdown]');
                this.waitUntilVisible('#example-dropdown');
                this.mouse.move('#example-dropdown .aui-dropdown2-radio');
                phantomcss.screenshot('#simple-example', 'interactive_dropdown__simple');
            });
        },
        'docs/flag.html': function(casper, test, phantomcss) {
            casper.evaluate(effectivelyDisabledAnimations);
            casper.then(function() {
                this.click('#next-flag-show-button');
            });
            casper.then(function() {
                this.click('#next-flag-show-button');
            });
            casper.then(function() {
                this.click('#next-flag-show-button');
            });
            casper.then(function() {
                this.click('#next-flag-show-button');
            });
            casper.then(function() {
                this.click('#next-flag-show-button');
            });
            casper.waitFor(function () {
                return this.evaluate(function () {
                    return document.querySelectorAll('#aui-flag-container .aui-flag').length === 5;
                });
            });
            casper.then(function() {
                phantomcss.screenshot('#aui-flag-container', 'flags');
            });
        },
        /*
        'docs/auiselect2.html': function(casper, test, phantomcss) {
            casper.then(function() {
                this.click('#s2id_select2-example input');
                this.waitUntilVisible('.select2-drop');
                phantomcss.screenshot('#simple-example', 'interactive_aui-select2__open');
                this.sendKeys('#s2id_select2-example input', casper.page.event.key.Down, { keepFocus: true });
                this.sendKeys('#s2id_select2-example input', casper.page.event.key.Enter, { keepFocus: true });
                this.sendKeys('#s2id_select2-example input', casper.page.event.key.Down, { keepFocus: true });
                this.sendKeys('#s2id_select2-example input', casper.page.event.key.Enter, { keepFocus: true });
                phantomcss.screenshot('#simple-example', 'interactive_aui-select2__selected');
            });
        },
        */
        'docs/progress-indicator.html': function(casper, test, phantomcss) {
            casper.evaluate(effectivelyDisabledAnimations);
            casper.then(function() {
                phantomcss.screenshot('#progress-example-20pct', 'progressbar-20pct');
            });
        },
    }
);
