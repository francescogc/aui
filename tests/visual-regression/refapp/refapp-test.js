/* eslint-env node */
/* global casper */
var phantomcss = require('phantomcss');

// Static elements
require('../visual-regression.js')(casper, phantomcss, 'refapp').run(
    'http://localhost:9999/ajs/plugins/servlet/ajstest/test-pages/pages',
    {
        'experimental/avatar/sizes/': ['#content'],
        'auiBadge/': ['#content'],
        'experimental/buttons/': ['#content'],
        // 'forms/default/': ['#content'],
        // 'forms/fieldsAndStates/': ['#content'],
        // 'forms/topLabels/': ['#content'],
        'i18n/fontStacks/': ['#content'],
        'icons/': ['#content'],
        'experimental/lozenges/': ['#content'],
        'experimental/pageLayout/layouts/navigation/default/': ['#content'],
        'experimental/pageLayout/layouts/groups/': ['#content'],
        'experimental/pageLayout/header/auiHeader/': ['#nav1', '#nav2', '#nav3', '#nav4'],
        'experimental/pageLayout/header/pageHeaderVariations/': ['#content'],
        'tables/': ['#basic', '#nested'],
        'toolbar/': ['#content'],
        'experimental/toolbar2/': ['#content']
    }
);

// Interactive elements
require('../visual-regression.js')(casper, phantomcss, 'components').run(
    'http://localhost:9999/ajs/plugins/servlet/ajstest/test-pages/pages',
    {
        /*
        // Disabled because the restfultable never seems to appear in CI, or
        // Casper is unable to locate the element.
        // The test is not so valuable as to block the entire build from passing.
        'restfultable/': function(casper, test, phantomcss) {
            casper.waitUntilVisible('#contacts-table.aui-restfultable');
            casper.waitWhileVisible('#contacts-table.loading');
            casper.then(function() {
                phantomcss.screenshot('#contacts-table', 'restfultable');
            });
        },
        */
        'experimental/datePicker/': function(casper, test, phantomcss) {
            casper.then(function() {
                this.click('#test-default-always');
            });
            casper.waitUntilVisible('.aui-datepicker-dialog');
            casper.then(function() {
                phantomcss.screenshot('.aui-datepicker-dialog', 'datepicker');
            });
        },
        /*
        // Disabled because the dialog doesn't appear in PhantomJS, plus the youtube video
        // fails to load, so generates animated static, which makes this always change+fail in CI.
        'dialog/': function(casper, test, phantomcss) {
            casper.then(function() {
                this.click('#dialog-button');
            });
            casper.waitFor(function() {
                return this.evaluate(function() {
                    var dialogTest = document.getElementById('dialog-test');
                    return dialogTest.style.display === 'block';
                });
            });
            casper.then(function() {
                phantomcss.screenshot('body', 'dialog1');
            });
        },
        */
        'inlineDialog2/': function(casper, test, phantomcss) {
            casper.then(function() {
                this.click('a[aria-controls="inline-dialog2-17"]');
                this.click('a[aria-controls="inline-dialog2-16"]');
                this.click('a[aria-controls="inline-dialog2-help-3"]');
            });
            casper.then(function() {
                phantomcss.screenshot('body', 'inline-dialog2');
            });
        },
    }
);
