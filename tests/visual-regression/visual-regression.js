/* eslint-env browser */
function filenameFor(page, id) {
    return (page + '_' + id).replace(/\//g, '_');
}

function getVisregDir(folderName, subdirName) {
    var folders = ['.', 'visreg'];
    if (typeof subdirName === 'string' && subdirName.length) {
        folders.push(subdirName);
    }
    folders.push(folderName);
    return folders.join('/');
}

// eslint-disable-next-line no-undef
module.exports = function(casper, phantomcss, subdir) {
    return {
        run: function (baseUrl, tests) {
            casper.options.viewportSize = {
                width: 1024,
                height: 800
            };

            casper.start();

            Object.keys(tests).forEach(function(page) {
                var pageUrl = baseUrl + '/' + page;
                var whatToDo = tests[page];

                function generateWhatToDo(input) {
                    var selectors = [].concat(input);
                    return function checkSelectors(casper, test) {
                        selectors.forEach(function(selector) {
                            casper.then(function() {
                                test.assertExists(selector);
                                phantomcss.screenshot(selector, filenameFor(page, selector));
                            });
                        });
                    }
                }

                if (whatToDo instanceof Array) {
                    whatToDo = generateWhatToDo(whatToDo);
                }

                if (typeof whatToDo !== 'function') {
                    whatToDo = generateWhatToDo();
                }

                casper.test.begin('Screenshots for ' + page, function suite(test) {
                    phantomcss.init({
                        casper: casper,
                        screenshotRoot: getVisregDir('screenshots', subdir),
                        failedComparisonsRoot: getVisregDir('failures', subdir),
                        comparisonResultRoot: getVisregDir('results', subdir),
                        cleanupComparisonImages: false,
                        addIteratorToImage: false,
                    });

                    casper.thenOpen(pageUrl, function() {
                        test.assertHttpStatus(200);
                    });

                    whatToDo.call(undefined, casper, test, phantomcss);

                    casper.then(function() {
                        phantomcss.compareSession();
                    });

                    casper.run(function() {
                        test.done();
                    });
                });
            });
        }
    };
};
