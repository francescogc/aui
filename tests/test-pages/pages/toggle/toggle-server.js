define('aui-test/toggle-server', function(require, exports) {
    const sinon = require('sinon');

    exports.url = '/toggle';
    exports.server = sinon.fakeServer.create();
    exports.server.autoRespond = true;
    exports.server.autoRespondAfter = 200;

    exports.server.respondWith('POST', exports.url, [
        204,
        {},
        ''
    ]);
});
