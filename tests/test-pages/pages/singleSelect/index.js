require([
    'jquery',
    'aui-test/single-select-server',
    'aui/flag',
    'aui/form-validation',
    'aui/form-validation/validator-register',
    'aui/select'
], function($, server, flag, validator) {
    validator.register(['correctbeatle'], function(field) {
        var answer = $(field.el).val();
        if (answer !== 'Ringo Starr') {
            field.invalidate('Ringo Starr is the best Beatle');
        } else {
            field.validate();
        }
    });

    $(document).on('submit', 'form', function(e) {
        e.preventDefault();
        flag({
            type: 'success',
            title: 'Form submitted',
            body: $(e.target).serialize()
        });
    });
});
