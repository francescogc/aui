define('aui-test/dropdown-server', function(require) {
    const sinon = require('sinon');
    const {
        serverResponse,
        noSectionLabelResponse,
        opensSubmenuResponse,
    } = require('aui-test/dropdown-fixtures');

    const server = sinon.fakeServer.create();
    server.autoRespond = true;
    server.autoRespondAfter = 2000;

    server.respondWith(/standard-async-dropdown/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(serverResponse)
    ]);

    server.respondWith(/no-section-label/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(noSectionLabelResponse)
    ]);

    server.respondWith(/opens-submenu/, [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(opensSubmenuResponse)
    ]);

    return server;
});
