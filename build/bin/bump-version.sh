#!/usr/bin/env bash
##
# A utility function for bumping the versions of our deliverables in our monorepo
#
basedir=$(pwd)
auidir=${basedir}

cd ${auidir}
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-docs
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-p2-plugin
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-p2-harness
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-workspace
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-flatapp
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-integration-test-suite
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-visual-regression-test-suite
yarn yarn-bump --dir ${auidir} --new-version $1 --package @atlassian/aui-soy

cd ${auidir}/p2
mvn versions:set --batch-mode -DnewVersion=$1 -DgenerateBackupPoms=false
