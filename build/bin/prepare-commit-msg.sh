#!/bin/sh

ISSUE_KEY=$(git symbolic-ref --short HEAD | awk -F'[-/]' '{print $1}')
ISSUE_NUMBER=$(git symbolic-ref --short HEAD | awk -F'[-/]' '{print $2}')
ISSUE="$ISSUE_KEY-$ISSUE_NUMBER"
ISSUE_IN_COMMIT=$(cat $1 | grep "^[^#\;]" | grep -c "$ISSUE")

if ([ "AUI" = "$ISSUE_KEY" ] || [ "DP" = "$ISSUE_KEY" ]) && ! [[ $ISSUE_IN_COMMIT -ge 1 ]]; then
    echo "[$ISSUE] $(cat $1)" > $1
fi
