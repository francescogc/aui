const { DefinePlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const project = require('../../package.json');

const { NODE_ENV } = process.env;

exports.setAuiVersion = () => ({
    plugins: [
        new DefinePlugin({
            AUI_VERSION: JSON.stringify(project.version || 'UNKNOWN')
        })
    ]
});

exports.transpileJs = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options,
                },
            },
        ]
    }
});

exports.extractCss = ({ include, exclude, options } = {}) => {
    const extractCSS = new MiniCssExtractPlugin(options);
    const autoprefixer = require('autoprefixer');

    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    include,
                    exclude,

                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                plugins: [
                                    autoprefixer
                                ]
                            }
                        },
                        'less-loader'
                    ]
                }
            ],
        },

        plugins: [
            extractCSS
        ],
    }
};

exports.inlineCss = () => {
    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    use: [
                        'style-loader',
                        'css-loader',
                        'less-loader'
                    ]
                }
            ]
        }
    };
};

exports.loadFonts = ({ include, exclude, options } = {}) => {
    return {
        module: {
            rules: [
                {
                    test: /\.(ttf|eot|woff|woff2)$/,
                    include,
                    exclude,
                    use: {
                        loader: 'file-loader',
                        options,
                    },
                }
            ]
        }
    };
};

exports.loadImages = ({ options } = {}) => {
    let imageMatcher = /\.(png|jpg|gif|svg)$/;
    let brandAndIconMatcher = /[\\/](?:.*?-logos|logos|fonts)[\\/]/;

    return {
        module: {
            rules: [
                {
                    test: imageMatcher,
                    exclude: brandAndIconMatcher,
                    use: {
                        loader: 'url-loader',
                        options,
                    },
                },
                {
                    test: imageMatcher,
                    include: brandAndIconMatcher,
                    use: {
                        loader: 'file-loader',
                        options,
                    }
                }
            ],
        },
    };
};

exports.loadSoy = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.soy$/,
                include,
                exclude,
                use: {
                    loader: '@atlassian/atlassian-soy-loader',
                    options,
                },
            },
        ]
    },
    plugins: [
        new DefinePlugin({
            'goog.DEBUG': false // will allow dead code removal of debugging code from generated templates
        })
    ]
});

exports.resolveSoyDeps = () => ({
    module: {
        rules: [
            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/atlassian-deps.js'),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'atl_soy'
                    }
                ]
            },
            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/soyutils.js'),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'soy,soydata,goog'
                    }
                ]
            }
        ]
    },
});

exports.production = () => {
    const definePlugin = new DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(NODE_ENV)
        }
    });

    if (NODE_ENV !== 'production') {
        return {
            mode: 'development',
            devtool: 'cheap-module-source-map',

            plugins: [
                definePlugin
            ]
        };
    }

    const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
    const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

    return {
        mode: 'production',
        devtool: 'source-map',

        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: true,
                    extractComments: {
                        banner: false
                    }
                }),
                new OptimizeCSSAssetsPlugin({
                    cssProcessorOptions: {
                        safe: true,
                        discardComments: {
                            removeAll: true
                        }
                    },
                    canPrint: true
                }),
            ]
        },

        plugins: [
            definePlugin,
        ]
    }
};
