'use strict';

var gulp = require('gulp');
var eslint = require('gulp-eslint');
var opts = require('gulp-auto-task').opts();

module.exports = function lintPreCommit () {
    return gulp.src(opts.files.split(','))
        .pipe(eslint())
        .pipe(eslint.format())
        .on('error', function () { /* noop */ })
        .pipe(eslint.failAfterError());
};
