/* global console */
import Backbone from 'our/backbone';
export default Backbone;

console.warn && console.warn('Use of `window.Backbone` through AUI is deprecated and will be removed in AUI 9.0');

let ourBackbone = Backbone;
window.Backbone = ourBackbone;
