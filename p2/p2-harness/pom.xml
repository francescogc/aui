<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.auinext</groupId>
        <artifactId>workspace</artifactId>
        <version>8.3.2-SNAPSHOT</version>
    </parent>
    <artifactId>p2-harness</artifactId>

    <name>AUI Next - Test harness</name>
    <packaging>atlassian-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.aui</groupId>
            <artifactId>auiplugin</artifactId>
            <version>${project.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>

        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <version>${webresource.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>jquery</artifactId>
            <version>${jquery.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <version>${soy.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <version>1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>sinonjs</artifactId>
            <version>1.17.2</version>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>${root.location}packages/soy/src</directory>
            </resource>
            <resource>
                <directory>${root.location}tests/test-pages</directory>
            </resource>
            <resource>
                <directory>src/main/resources/test-pages</directory>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-refapp-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${refapp.version}</productVersion>
                    <httpPort>${http.port}</httpPort>
                    <jvmDebugPort>8905</jvmDebugPort>
                    <jvmDebugSuspend>false</jvmDebugSuspend>
                    <contextPath>${context.path}</contextPath>
                    <closureJsCompiler>false</closureJsCompiler>
                    <compressJs>false</compressJs>
                    <compressResources>false</compressResources>
                    <enableFastdev>false</enableFastdev>
                    <enableQuickReload>true</enableQuickReload>
                    <quickReloadVersion>2.0.0</quickReloadVersion>

                    <systemPropertyVariables>
                        <atlassian.disable.caches>false</atlassian.disable.caches>
                        <atlassian.dev.mode>true</atlassian.dev.mode>
                    </systemPropertyVariables>

                    <pluginArtifacts>
                        <pluginArtifact>
                            <groupId>com.atlassian.soy</groupId>
                            <artifactId>soy-template-plugin</artifactId>
                            <version>${soy.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>jquery</artifactId>
                            <version>${jquery.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.aui</groupId>
                            <artifactId>auiplugin</artifactId>
                            <version>${project.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>atlassian-plugins-webresource-plugin</artifactId>
                            <version>${webresource.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>atlassian-plugins-webresource-rest</artifactId>
                            <version>${webresource.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>
                    <libArtifacts>
                        <libArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>atlassian-plugins-webresource-api</artifactId>
                            <version>${webresource.version}</version>
                        </libArtifact>
                        <libArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>atlassian-plugins-webresource-spi</artifactId>
                            <version>${webresource.version}</version>
                        </libArtifact>
                        <libArtifact>
                            <groupId>com.atlassian.plugins</groupId>
                            <artifactId>atlassian-plugins-webresource</artifactId>
                            <version>${webresource.version}</version>
                        </libArtifact>
                    </libArtifacts>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                    </execution>
                </executions>
                <configuration>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.atlassian.plugin</groupId>
                            <artifactId>atlassian-spring-scanner-external-jar</artifactId>
                        </dependency>
                    </scannedDependencies>
                    <verbose>false</verbose>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <atlassian.spring.scanner.version>1.2.13</atlassian.spring.scanner.version>
        <soy.version>4.5.0</soy.version>
        <webresource.version>3.6.1</webresource.version>
        <jquery.version>3.3.1.1</jquery.version>

        <!-- Test harness stuff -->
        <http.port>9999</http.port>
        <context.path>/ajs</context.path>

        <!-- We have some external dependencies that end up in the plugin file system
             and need manual translating in to web-resources. -->
        <sinonjs.output.dir>META-INF/resources/webjars/sinonjs/1.17.2/sinon.js</sinonjs.output.dir>

        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
        <root.location>${basedir}/../../</root.location>
    </properties>

</project>
