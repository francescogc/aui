# 8.3.1
* [Documentation](https://aui.atlassian.com/8.3.1/)

## Highlights
Added new message type - change

## Deprecated
We've deprecated success message type - confirmation should be used instead

# 8.3.0
* [Documentation](https://aui.atlassian.com/8.3.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=8.3.0)

## Upgrade notes
* Read the upgrade guide

## Highlights
* Message component was updated and it's appearance aligned with ADG3.
* Colors of lozenges was updated and aligned with ADG3.

## Changed
* Messages:
    * padding was changed from 20px to 15px
    * border was removed - for now we left border definition and set border width to 0
    * background is no longer white and it's reflecting type of message, background colors are aligned with Section Message from Atlaskit
    * icon colors are updated to darker ones (G400, Y400, R400)
* Lozenges:
    * text color and background was changed to align with ADG3
    
## Added
We've added new class names for lozenges types to align with ADG3 and Atlaskit:
* aui-lozenge-inprogress should be used in place of aui-lozenge-current
* aui-lozenge-removed should be used in place of aui-lozenge-error

## Deprecated
We've deprecated class names for lozenge types:
* aui-lozenge-current - aui-lozenge-inprogress should be used instead
* aui-lozenge-error - aui-lozenge-removed should be used instead

# 8.2.0
* [Documentation](https://aui.atlassian.com/8.2.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=8.2.0)

## Upgrade notes
* Read the upgrade guide for a checklist of things to review in your own JavaScript when updating to this version of AUI.

## Highlights
* AUI's layered elements are powered by [Popper.js](https://popper.js.org/) instead of [Tether](http://tether.io/).
* Several buggy layer behaviours have been resolved.

## Added
* It is now possible to use form controls like AUI Select2 and single-select inside an inline dialog.

## Changed
* Layered elements position themselves asynchronously in a `requestAnimationFrame`.
* Dropdowns and inline dialogs can now be controlled by multiple trigger elements.
* Dropdowns and inline dialogs now align with the last trigger to activate them.
* Modal dialogs have an improved focus trap that allows focus to move to child layer elements.

## Fixed
* Contents of dropdowns, inline dialogs, modal dialogs, and single select suggestions no longer become blurry if they are resized or the page is zoomed.
* Dropdowns, inline dialogs, and single select suggestions stay aligned with their triggering element in more cases, such as:
    * When the page is scrolled or resized;
    * When the trigger is `position:sticky`;
    * When the trigger is inside a scrollable container (i.e., an element with a custom `overflow` and `height` or `width`).
* Inline dialogs are no longer partially clipped by the viewport when opened on the edge of the viewport.
* Single select suggestions should no longer disappear off the page.
* Modal dialogs, when stacked, now close in the correct order.
* Menus for a collapsed sidebar now persist if the page is scrolled.

## Removed
* The `aui-alignment-element-attached-` and `aui-alignment-target-attached-` CSS classes are no longer added to layered elements.

# 8.1.0
* [Documentation](https://aui.atlassian.com/8.1.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=8.1.0)

## Upgrade notes
* If you use `AJS.template`, `AJS.messages`, `AJS.flag`, or `AJS.banner`,
  and are passing `<script>` tags in to their html properties, you should
  convert the scripts to separate discrete files and load them in an alternate
  way.

## Added
* The "app-switcher" icon glyph was redesigned.

### Node distribution
* A new `aui-prototyping.nodeps.js` was added, which excludes the Backbone and Underscore libraries.
  Consumers must first ensure that jQuery, Backbone, and Underscore are available before this bundle is loaded.

## Changed
* The `AJS.template` function will strip `<script>` tags from any html property it fills in.
* `AJS.messages`, `AJS.flag`, and `AJS.banner` will strip `<script>` tags from their respective
  body html attributes.
* A disabled AUI button has an alpha-transparent background instead of a solid colour.
* The `aui-buttons` web-resource will now pull in the code to make [spinners in buttons](https://docs.atlassian.com/7.10.0/docs/buttons.html#button-spinners) work.
* The icon glyphs for "approve", "check-circle-filled", "link", "link-filled", "search", and "unlink" were revised.
* Removed deprecation warning for the `aui-iconfont-appswitcher` and `aui-iconfont-app-switcher` CSS classes.

### Node distribution
* The `aui-prototyping.js` bundles' UMD module name is now `@atlassian/aui` instead of `aui`, in order to
  match its package name on NPM. This improves compatibility with various build tools and module loaders.

## Fixed
* Spinner elements no longer appears over the top of other layered elements, such as a flag or dialog.
* Toggle elements are more resilient to mutation events.
* Closing an AUI message no longer fires the close event multiple times.
* The close icon in an AUI Select2 now renders correctly.

# 8.0.0
* [Documentation](https://aui.atlassian.com/8.0.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=8.0.0)

## Highlights

* AUI's source is divided in to multiple discrete Node packages.
* AUI's source is valid ES6, and can be compiled via bundlers like Webpack, Rollup, and Parcel.
* The core of AUI is now released under an open-source licence. Atlassian-specific design assets are still licensed under the ADG license.
* The default entry points for AUI pull in significantly less code than in previous versions.
* Several long-deprecated components and helpers have been removed from AUI.
* Several jQuery plugins have been removed from AUI.
* Backbone and Underscore are now considered a deprecated implementation detail of AUI and may not ship in future major versions.
* jQuery UI is now considered a deprecated implementation detail of AUI and may not ship in future major versions.
* We removed last usage of Soy templates in our codebase thus making our production code no longer dependent on Soy.

## Added
* AUI now supports jQuery 3+ natively. No need for the jquery-migrate plugin here!

### Atlassian-Plugin
* Several of jQuery UI's behavioural components are available as discrete web-resources.
  Check the upgrade guide for details.

## Changed
* AUI's version of jquery-ui has been bumped to v1.12.1, up from v1.8.24.
* AUI's version of Backbone has been bumped to v1.3.3, up from v1.0.0.
* AUI's version of Underscore has been bumped to v1.9.2, up from v1.5.2.
* AUI's version of Tipsy has been changed to use [Atlassian's fork of Tipsy](https://www.npmjs.com/package/@atlassian/tipsy), which supports jQuery 3+.
* The UI components from jQuery UI are no longer provided via AUI.

### JavaScript
* The minimum supported version of `jQuery` is now v2.2.4, up from v1.7.2.
* `window.AJS` is now an object, not a function.
* `AJS.toInit` always runs callbacks asynchronously.

### CSS
* We removed vertical-align: text-top override for .aui-icon-small and .aui-icon-large so text-bottom will be new default

### Node distribution
* The Node distribution's shape has changed significantly.
    * Previously we shipped a lot of images and other assets that either weren't directly referenced in our CSS files,
      or were inlined in our CSS. Now, because our assets are compiled through webpack, the only other external
      assets we put in the `dist/` folder are guaranteed to be used by the bundle.
    * In 8.0, we are only shipping a single bundle, intended for prototyping:
      `aui.prototyping.js` and `aui.prototyping.css`.
    * Future versions of 8.x will add additional bundles based on an understanding of AUI's consumer's use-cases.
    * Future versions of 8.x will aim to make it possible for developers to build their own bundles.
* Distribution bundles are now exported using [the UMD pattern](https://github.com/umdjs/umd).
* Distribution bundles will not define `window.AJS` if an AMD loader is present.
* AUI's CSS deprecations bundle is immediately executed upon loading the file.
* See the upgrade guide for more details on the Node distribution changes.

### Atlassian-Plugin
* The minimum supported version of [Atlassian Web Resources (aka WRM)][WRM] is v3.6.0, up from v3.3.0.
* The `aui-core` web-resource ships far less code by default.
  Requiring the `aui-core` key gives you:
    * `AJS.version`
    * `AJS.bind`, `AJS.unbind`, and `AJS.trigger`
    * `AJS.escapeHtml`
    * `AJS.format`
* The `ajs` web-resource was overhauled significantly to reduce its impact on page weight.
  Requiring the `ajs` key gives you:
    * CSS reset, base typography, and basic HTML element styles (e.g., tags from `<a>` to `<strong>`)
    * Utility CSS classes (e.g., `.assistive` and `.hidden`)
    * The page layout and page header CSS patterns
    * The avatar, button, group+item, and toolbar2 CSS patterns
    * The forms and form field patterns
    * All of `aui-core`, plus:
        * `AJS.$` (a locally-cached version of `jQuery`)
        * `AJS.toInit`
        * `AJS.LayerManager.global`
  All other AUI patterns and components can be required by explicitly depending upon their
  respective web-resource keys.
* AUI traditionally shipped several web-resources for importing parts of jQuery UI.
  Now, it only ships explicit web-resource keys for select pieces of jQuery UI's behaviour.
  Check the upgrade guide for specifics.
* The deprecated `jQuery.fn.spin` and `jQuery.fn.spinStop` were split in to their own web-resource: `jquery-spin`.
  The `aui-spinner` web-resource only ships the web component.

## Deprecated
* The `window.Backbone` global has been deprecated.
* The `window._`. global has been deprecated.
* `AJS.debounce` and `AJS.debounceImmediate` have been deprecated.
  Equivalent functions from libraries like lodash and underscore should be used instead.

### Atlassian-plugin
* Several web-resource keys have changed names.
  The old names are deprecated and will log a warning to the server's logs when used.
* Most "bundle" web-resource keys are now deprecated.
  Developers should favour pulling in the smallest possible web-resource they can
  in order to keep their code dependencies manageable and their apps' page weight low.
  The following web-resource keys are deprecated:
    * `ajs-gadgets`
    * `ajs-gadgets-base`
    * `aui-page-suite`
    * `internal-jquery-ui-partial`
    * `jquery-ui-other`
    * `jquery-ui-interactions`
  Check the upgrade guide for alternatives for each of these web-resources.
* The "template" web-resource and `AJS.template` function are deprecated.

## Removed
* All resources suffixed with "-legacy" have been removed.
* CSS and JS code to support IE 10 and lower has been removed.

### Components and patterns
* The `shadowed` CSS class for AUI's message pattern has been benign for some time, so its associated
  configuration options in JavaScript and Soy have been removed.
* The `unsectioned` CSS class for AUI's forms pattern has been benign for some time, so its associated
  configuration in Soy has been removed.

### JavaScript
* `AJS()` has been removed. There are better ways to create DOM elements.
* The `Binder` class has been removed.
* The polyfill for the `placeholder` attribute on `<input>` elements has been removed.
* The polyfill for `requestAnimationFrame` has been removed.
* The polyfill for `console` has been removed.
* The following functions have been removed from AUI's `browser` module:
    * `supportsRequestAnimationFrame`
    * `supportsCssTransition`
    * `supportsCalc`
    * `supportsNewMouseEvent`
* The following jQuery plugins have been removed:
    * `$.os`
    * `$.fn.autocomplete`
    * `$.fn.isDirty`
    * `$.fn.progressBar`
    * `$.fn.selection`
    * `$.fn.selectionRange`
    * `$.fn.wrapSelection`
* `AJS.bindEvt`, `AJS.triggerEvt`, and `AJS.triggerEvtForInst` have been removed.
* `AJS.firebug` and `AJS.warnAboutFirebug` have been removed.
* `AJS.filterBySearch` has been removed.
* `AJS.include` has been removed.
* `AJS.message.template` has been removed.
* `AJS.preventDefault` and `AJS.stopEvent` have been removed.
* `AJS.isVisible`, `AJS.setCurrent` and `AJS.setVisible` have been removed.
* `AJS.toggleClassName` has been removed.

### CSS
* The `-hero` and `-marketing` variants of header sizes have been removed.
* The `.aui-zebra` CSS class has been removed.
* The `.aui-nav-current` CSS class has been removed. Use `.aui-nav-selected` instead.
* The `.vertical-tabs.aui-legacystyle2011` CSS class has been removed.
* The `.aui-box-shadow` CSS class has been removed.
  * Use the mixins available in `shadows.less` instead.
* The `.aui-dropdown-icon` CSS class has been removed (it was unneeded; the dropdown trigger pattern itself adds a psuedo-element for its icon).
* The un-prefixed AUI message classes -- `success`, `hint`, `warning`, `info`, and `error` -- have been removed.
* Old "messages" icons CSS classes have been removed:
    * `.aui-icon-success`
    * `.aui-icon-warning`
    * `.aui-icon-info`
    * `.aui-icon-error`
    * `.aui-icon-hint`
    * `.aui-icon-generic`
    * `.icon-generic`
    * `.icon-error`
    * `.icon-hint`
    * `.icon-info`
    * `.icon-success`
    * `.icon-warning`
* Old "gadget" icons CSS classes have been removed:
    * `.icon-dropdown`
    * `.icon-dropdown-active`
    * `.icon-dropdown-active-d`
    * `.icon-maximize`
    * `.icon-maximize-d`
    * `.icon-minimize`
    * `.icon-minimize-d`
    * `.icon-move`
    * `.icon-move-d`

### Node distribution
* The `lib/` folder has been removed for now, pending use-cases.
* Several assets have been removed from the `dist/` folder.
    * Several images and other old assets that weren't directly referenced in AUI's source have been removed.
    * The `dist/js/aui.js` and `dist/css/aui.js` bundle has been removed. See the upgrade guide for its replacement.
    * The `dist/js/aui-experimental.js` and `dist/css/aui-experimental.js` bundle has been removed. See the upgrade guide for its replacement.
* The `aui/aui-css-deprecations` AMD module has been removed, since the bundle is immediately executed upon loading the file.

### Atlassian-Plugin
* Source files have been removed from the plugin.
* All "soy-legacy1" templates and legacy soy web-resource keys have been removed.
* All "-legacy1" web-resource keys have been removed.
* Several "jquery-" web-resource keys have been removed, including:
    * `jquery-all`
    * `jquery-autocomplete-deprecated`
    * `jquery-compatibility`
    * `jquery-effects`
    * `jquery-lib`
    * `jquery-progressbar`
    * `jquery-selection`
    * `jquery-ui-widgets`
* The "deprecated-legacy-images" web-resource and associated image sources have been removed.
* The "ajs-html5shim" and "modernizr-touch" web-resources have been removed.
* The "ajs-raf" web-resource has been removed.
* The "aui-experimental-header-rotp" web-resource has been removed.
* All Java class files have been removed. These behaviours are provided by [the Atlassian Web Resource plugin][WRM].

# 7.10.0
* [Documentation](https://docs.atlassian.com/aui/7.10.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.10.0)

## Upgrade notes
* If you use `AJS.template`, `AJS.messages`, `AJS.flag`, or `AJS.banner`,
  and are passing `<script>` tags in to their html properties, you should
  convert the scripts to separate discrete files and load them in an alternate
  way.

## Added
* The "app-switcher" icon glyph was redesigned.

## Changed
* The `AJS.template` function will strip `<script>` tags from any html property it fills in.
* `AJS.messages`, `AJS.flag`, and `AJS.banner` will strip `<script>` tags from their respective
  body html attributes.
* A disabled AUI button has an alpha-transparent background instead of a solid colour.
* The `aui-buttons` web-resource will now pull in the code to make [spinners in buttons](https://docs.atlassian.com/7.10.0/docs/buttons.html#button-spinners) work.
* The icon glyphs for "approve", "check-circle-filled", "link", "link-filled", "search", and "unlink" were revised.
* Removed deprecation warning for the `aui-iconfont-appswitcher` and `aui-iconfont-app-switcher` CSS classes.

## Fixed
* Spinner elements no longer appears over the top of other layered elements, such as a flag or dialog.
* Toggle elements are more resilient to mutation events.
* Closing an AUI message no longer fires the close event multiple times.
* The close icon in an AUI Select2 now renders correctly.

# 7.9.9
* [Documentation](https://docs.atlassian.com/aui/7.9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.9)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11. 

# 7.9.8
## Upgrade notes
* This release is the exact same as 7.9.7. It pays to push changes before releasing :P

# 7.9.7
* [Documentation](https://docs.atlassian.com/aui/7.9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.7)

## Fixed
* Tooltips on sidebar links correctly hide and will not prevent clicking content underneath them.
    * Tooltips on sidebar links will once again flicker when hovering over the link's badge. The bug will be re-opened.

# 7.9.6
* [Documentation](https://docs.atlassian.com/aui/7.9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.6)

## Fixed
* Tooltips on sidebar links will not flicker when hovering over the link's badge.
* The resources are minified again in the P2 plugin.

# 7.9.5
* [Documentation](https://aui.atlassian.com/7.9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.5)

## Fixed
* When the (deprecated) `jQuery.fn.spinStop()` function is called, it also cleans up its cached data.

# 7.9.4
* [Documentation](https://aui.atlassian.com/7.9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.4)

## Added
* The spinner documentation page now has more usage examples of `<aui-spinner>`.
* The button documentation page demonstrates use of its `busy` state.

## Changed
* `<aui-spinner>` is used directly in the `busy` state of the button component.
* `<aui-spinner>` is used directly in AUI toggle's asynchronous variant.

## Deprecated
* The `filled` attribute of `<aui-spinner>` is deprecated. The element should be positioned using CSS.

# 7.9.3
* [Documentation](https://aui.atlassian.com/7.9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.3)

## Changed
* The (deprecated) `jQuery.fn.spin()` function now creates the `<aui-spinner>` element synchronously.
    * This means the element's attributes and prototype can be inspected and affected immediately after calling the function.

## Fixed
* An `<aui-spinner>` element will correctly position itself after multiple renders or when re-attached to the DOM.
* Spinners with no (or an invalid) size attribute will render at the default `medium` size.
* Calling the (deprecated) `jQuery.fn.spin(true)` will create a spinner with a `small` size.

# 7.9.2
* [Documentation](https://aui.atlassian.com/7.9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.2)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even
  if they exceed the `max`. This cached value will be used if `max` is updated to accommodate it.
  This is in line with the HTML5 `<progress>` element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.

## Fixed

* `RestfulTable`'s `comparator` function (for sorting rows) now runs with the correct `this` context.
* `RestfulTable.EntryModel` no longer appends `undefined` to the URL for deleting a row.
* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.9.1
* [Documentation](https://aui.atlassian.com/7.9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.1)

## Changed

* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Documentation fixes:
    * The Buttons page lists the correct CSS class to add to a cancel button.
    * Typing in to the search bar on the iconography page once again visually dims non-matching icons.

# 7.9.0
* [Documentation](https://aui.atlassian.com/7.9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.0)

## Highlights

* AUI's various drop down menus have been updated to reflect the latest [ADG Server](https://atlassian.design/server) guidelines.

## Added

* The `<aui-item-link>`, `<aui-item-checkbox>` and `<aui-item-radio>` elements now allow setting an `item-id` attribute to place an `id` on the nested interactive element.

## Changed

* Items in AUI's various drop down menus are now a minimum of 30px tall.
* Sub-menu triggers are represented by a chevron glyph instead of a faux arrow.
* Sections in `<aui-dropdown-menu>` elements now have whitespace between them.

## Fixed

* Shadows of AUI's various drop down menus and dialogs will render more visibly in IE 11.
* Single selects with placeholder values will show their dropdown lists when clicked in IE 11.
* AUI's buttons now correctly compute their line-height value to be an effective 30px.
* Re-introduced ability to call `jQuery(element).spin(false)` to hide a spinner. This is equivalent to calling `jQuery(element).spinStop()`.
* Spinners inside an asynchronous drop down are now positioned correctly.
* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.8.3
* [Documentation](https://docs.atlassian.com/aui/7.8.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.3)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11. 

# 7.8.2
* [Documentation](https://aui.atlassian.com/7.8.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.2)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even
  if they exceed the `max`. This cached value will be used if `max` is updated to accommodate it.
  This is in line with the HTML5 `<progress>` element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.
* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.8.1
* [Documentation](https://aui.atlassian.com/7.8.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.1)

## Fixed

* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.8.0
* [Documentation](https://aui.atlassian.com/7.8.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.0)

## Highlights
* AUI's use of colour and typography have been updated to reflect the latest [ADG Server](https://atlassian.design/server) guidelines.
* AUI's tables patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.

## Added
* A new `aui-table-list` class has been added for styling tables. See the [tables](https://aui.atlassian.com/7.8.0/docs/tables.html) page for an example.
* There are now explicit web-resource keys for a few components and patterns:
    * Basic form field CSS: `com.atlassian.auiplugin:aui-forms`
    * Basic table CSS: `com.atlassian.auiplugin:table`
    * The Tabs pattern: `com.atlassian.auiplugin:tabs`

## Changed
* Table header styles now match the ADG Server guidelines.
* Avatar images now take up 100% of the element's available width and height, stretching non-square images as necessary.
* The AUI P2 plugin is now [transformerless](https://developer.atlassian.com/server/framework/atlassian-sdk/configuration-of-instructions-in-atlassian-plugins/), so should install and start a bit faster.
* The AUI P2 plugin requires Spring Scanner `2.0.0` or higher in order to run.

## Fixed
* AUI's legacy toolbar applies the same height to `<a>`, `<input>`, and `<button>` elements with the `.toolbar-trigger` class applied to them.
* Avatars using SVG for their image content will now render in IE 11.

# 7.7.6
* [Documentation](https://docs.atlassian.com/aui/7.7.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.6)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11. 

# 7.7.5
* [Documentation](https://aui.atlassian.com/7.7.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.5)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even
  if they exceed the `max`. This cached value will be used if `max` is updated to accommodate it.
  This is in line with the HTML5 `<progress>` element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.
* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.7.4
## Upgrade notes
* This release is broken. Use 7.7.5 instead.

# 7.7.3
* [Documentation](https://aui.atlassian.com/7.7.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.3)

## Fixed

* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.7.2
* [Documentation](https://aui.atlassian.com/7.7.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.2)

## Fixed
* Spinner now works in IE 11.
* Toggle button's `change` event fires in IE 11.
* Toggle button no longer fires a `change` event when it is first rendered.

# 7.7.1
* [Documentation](https://aui.atlassian.com/7.7.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.1)

## Changed
* The colour usage in step names in the multi-step progress tracker have been tweaked.
* A light shade of grey was removed from the multi-step progress tracker's progress bar.

## Fixed
* AUI's Select2 version will now work in an environment with Content Security Policy (CSP) enabled.

# 7.7.0
* [Documentation](https://aui.atlassian.com/7.7.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.0)

## Highlights
* Several of AUI's components have been updated to match the [ADG Server](https://atlassian.design/server) guidelines, including:
    * [Progress indicator](https://aui.atlassian.com/7.7.0/docs/progress-indicator.html)
    * [Progress tracker](https://aui.atlassian.com/7.7.0/docs/progress-tracker.html)
    * [Spinner](https://aui.atlassian.com/7.7.0/docs/spinner.html)

## Added
* The AUI progress indicator is implemented as a web component: `<aui-progressbar>`.
* The AUI spinner is implemented as a web component: `<aui-spinner>`.

## Changed
* The sidebar now always renders inside a `requestAnimationFrame` callback, improving initial render time.
* The page footer now renders Atlassian's new brand logo.
* The Toggle Button element's `defaultValue` property now remains constant, which makes "dirty form" checking work.
* Styles for hyperlinks are now independently consumable from the page layout pattern:
    * (In CSS) via the `lib/css/aui-link.css` file; or
    * (In P2) via the `com.atlassian.auiplugin:aui-link` web-resource.
* Styles for `.aui-group` and `.aui-item` are now independently consumable from the page layout pattern:
    * (In CSS) via the `lib/css/aui-group.css` file; or
    * (In P2) via the `com.atlassian.auiplugin:aui-group` web-resource.

## Fixed
* AUI's [buttons](https://aui.atlassian.com/7.7.0/docs/buttons.html) documentation was updated to reference how to set them as "busy".
* AUI's [toggle button](https://aui.atlassian.com/7.7.0/docs/toggle-button.html) now fires a "change" event when its value changes.

## Deprecated
* Use of `AJS.progressBars.update` and `AJS.progressBars.setIndeterminate` are deprecated.
* Use of the `jquery.throbber` plugin via AUI is deprecated.
* (To reiterate a 6.1.0 deprecation notice): Consuming the Atlassian brand logos via AUI has been deprecated. Use [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) instead.

## Removed
* The [spin.js](https://github.com/fgnass/spin.js) library is no longer used by AUI and has been removed.
    * As a consequence, a global `Spinner` function is no longer provided by AUI.
* The `jquery.spin` plugin is no longer used by AUI and has been removed.

## Upgrade notes
* Read the upgrade guide for a checklist of things to review in your code when updating to this version of AUI.

# 7.6.2
* [Documentation](https://aui.atlassian.com/7.6.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.6.2)

## Fixed
* The order of buttons in sidebar footer and disappearing sidebar when scrolling.

# 7.6.0
* [Documentation](https://aui.atlassian.com/7.6.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.6.0)

## Highlights
* AUI's sidebar, vertical navigation, and horizontal navigation patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.

## Upgrade notes
* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.5.1
* [Documentation](https://aui.atlassian.com/7.5.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion%20IN%20(7.5.0%2C%207.5.1))

## Highlights
* AUI's iconography has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's icon fonts declare their @font-face separately from their usage.

## Changed
* Chevrons in the application header's menu items have been updated.

## Deprecated
* Some icon class names were deprecated, as they are no longer used in Atlassian products. Check the upgrade guide for details.

## Upgrade notes
* Read the upgrade guide for information about how the iconography changes may affect you.

# 7.5.0
## Upgrade notes
This release is broken. Please use 7.5.1 instead.

# 7.4.0
* [Documentation](https://aui.atlassian.com/7.4.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.4.0)

## Highlights
* AUI's avatar component has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's tab component has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* Primary buttons in the application header have been adjusted to match the [ADG Server](https://atlassian.design/server) guidelines.

## Fixed
* Badge components in the application header have an updated contrast so they can be seen.
* Non-primary buttons in the application header have an updated contrast so they can be seen.

## Removed
* Support for Internet Explorer versions 9 and 10 has been removed. IE 11 is now the oldest supported version.

## Upgrade notes
* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.3.4
* [Documentation](https://aui.atlassian.com/7.2.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.2.4)

## Fixed
* A modal dialog's height will resize fluidly with the browser instead of jumping to explicit fixed heights.

# 7.3.3
* [Documentation](https://aui.atlassian.com/7.3.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion%20IN%20(7.3.2%2C%207.3.3))

## Changed
* Links in an `.aui-nav-actions-list` now have a bullet between them.
* Modal dialog heading text size has shrunk slightly to accommodate the longer text we see in them. Y'all are more verbose than this changelog line!
* Modal dialog and most AUI components now have a consistent border-radius of 3px.

## Fixed
* Inline dialog components have seen the light; they cast a shadow once again.
* Hint text in modal dialog footers is more subtle so as not to distract from the dialog's contents.
* The `"browser"` field in AUI's Node package points to the right file path now, so things like webpack and [unpkg.com] will load the correct `aui.js` batched file by default.

# 7.3.2
## Upgrade notes
This release is broken. Please use 7.3.3 instead.

# 7.3.1
* [Documentation](https://aui.atlassian.com/7.3.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project%3DAUI%20and%20fixVersion%20IN%20(7.3.0%2C%207.3.1))

## Highlights
* AUI's modal dialog patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's flag and message patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* The AUI Soy templates for use in JavaScript are now compiled via Webpack.

## Fixed
* The checksum for AUI 7.3.0's Node package was incorrect and preventing installation. This has been fixed.

# 7.3.0
## Upgrade notes
This release is broken. Please use 7.3.1 instead.

# 7.2.0
* [Documentation](https://aui.atlassian.com/7.2.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.2.0)

## Highlights
* AUI's Button and Toolbar pattern have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* The buttons in Dialog1 and file upload fields have also been updated to match AUI's Button pattern.

# 7.1.0
* [Documentation](https://aui.atlassian.com/7.1.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.1.0)

## Highlights
* The [ADG Server](https://atlassian.design/server) colour and typography foundations have been applied to AUI.

## Added
* The `badge` component has a new `primary` sub-type.

## Upgrade notes
* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.0.0
* [Documentation](https://aui.atlassian.com/7.0.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.0.0)

## Upgrade notes
* AUI has been re-licensed under the [Atlassian Developer Terms](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/).

## Changed
* The AUI and AUI-ADG repositories have been merged in to a single repository.
* The build process for AUI has been drastically simplified.

# 6.1.3
* [Documentation](https://aui.atlassian.com/6.1.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.1.3)

## Fixed

* AUI's Select2 version will now work in an environment with Content Security Policy (CSP) enabled.

# 6.1.2

## Upgrade Notes
* This release is broken. Please use 6.1.3 instead.

# 6.1.1
* [Documentation](https://aui.atlassian.com/6.1.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.1.1)

## Added
* The soy template for creating a `<select>` element now accepts a `defaultOption` parameter for creating an additional selectable value. This is useful when the `options` array does not or cannot include the specific selectable value.

## Fixed
* Single select with `no-empty-values` resets to empty if no valid value was previously chosen; no longer sets selection as `undefined`.

# 6.1.0
* [Documentation](https://aui.atlassian.com/6.1.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.1.0)

## Added
* AUI is now mostly compatible with jQuery 2.x and jQuery 3.x.

## Changed
* Internal use of AUI's deprecated code has been removed. This results in a slightly faster UI and far fewer deprecation warnings!

## Deprecated
* Consuming the Atlassian brand logos via AUI has been deprecated. Use [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) instead.
* Deprecation warnings have been updated to specify a future version, where they will be removed. This is a somewhat meta-deprecation deprecation. Yo dawg.

# 6.0.9
* [Documentation](https://aui.atlassian.com/6.0.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.9)

## Added
* The soy template for creating a `<select>` element now accepts a `defaultOption` parameter for creating an additional selectable value. This is useful when the `options` array does not or cannot include the specific selectable value.

## Fixed
* Single select with `no-empty-values` resets to empty if no valid value was previously chosen; no longer sets selection as `undefined`.

## Changed
* Several documentation pages were updated for correctness.

# 6.0.8
* [Documentation](https://aui.atlassian.com/6.0.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.8)

## Fixed

* The message component's `title` parameter is now optional. If omitted, placeholder markup for the title is not generated.
* The `aui.css` flatpack file no longer requests `clear.svg` seperately; the image is inlined in the file.

## Changed

* Documentation examples now push to codepen by default, with jsfiddle as a secondary fallback.

# 6.0.7
* [Documentation](https://aui.atlassian.com/6.0.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.7)

## Fixed
* jquery.hotkey no longer incorrectly triggered from events in contenteditable containers.
* Documentation link to "ask a question" has been updated to point to [https://community.developer.atlassian.com].

# 6.0.6
* [Documentation](https://aui.atlassian.com/6.0.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.6)

## Upgrade Notes
* This release made a change to jquery.hotkeys, but introduced a regression of its behaviour for users without a `window.$` variable. Please use 6.0.7 instead.

# 6.0.5
* [Documentation](https://aui.atlassian.com/6.0.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.5)

## Fixed
* Responsive header now correctly opens submenus when the dropdown `id` contains special CSS characters.

# 6.0.4
* [Documentation](https://aui.atlassian.com/6.0.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.4)

## Fixed
* WhenIType supports there being no `MouseEvent` available
* i18n is overridable again by overriding AJS.i18n.
* Fix a bug where inline dialog's aria-expanded state would become out of sync with inline dialog's open state, when closed on outside click

# 6.0.2
* [Documentation](https://aui.atlassian.com/6.0.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.2)

## Fixed
* Tooltips with overflowing content would sometimes overflow over the triggering component.

# 6.0.1
* [Documentation](https://aui.atlassian.com/6.0.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.1)

## Fixed
* Repeated initialisation of keyboard shortcuts no longer duplicates text in title attributes.
* WhenIType now uses native events instead of jQuery events. This works better with Synthetic Events in React.
* Responsive (asnyc) header submenus now display when the header is initialised at a narrow screen width.

# 6.0.0
* [Documentation](https://aui.atlassian.com/6.0.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.0)

## Upgrade Notes
* The Raphael (`com.atlassian.auiplugin:ajs-raphael`) and Eve (`com.atlassian.auiplugin:ajs-evejs`) resources have been removed.
* The legacy Inline Dialog component had two deprecated methods removed which used Raphael to draw arrows/shadows.
* The draw-logo script was also removed from the legacy AJS resource.

## Removed
* Removed the Raphael and Eve web resources.
* Removed the `getArrowAttributes` and `getArrowPath` methods from the legacy Inline Dialog component.
* Removed the draw-logo resource used in the legacy AJS resource.
* Removed the SiteDarkFeatureEnabledUrlReadingCondition class.

# 5.10.1
* [Documentation](https://aui.atlassian.com/5.10.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.10.1)

## Added
* Header can now be loaded asynchronously by including the `aui-header-async` resource.
* `aui-header-async` web resource added for asynchronously loaded header.

## Removed
* AUI was erroneously exposing an `aui-header` test web component. This has been removed.

# 5.10.0
* [Documentation](https://aui.atlassian.com/5.10.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.10.0)

## Upgrade Notes
* This release is broken. Please use 5.10.1 instead.

# 5.9.24
[Documentation](https://aui.atlassian.com/5.9.24/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.24)

## Fixed
* Validator again exposes AJS.I18n and AJS.format

# 5.9.23
[Documentation](https://aui.atlassian.com/5.9.23/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.23)

## Fixed
* Date validator now correctly handles dates in October
* Added missing 'aui-dropdown2-in-header' and 'aui-dropdown2-in-buttons' classes to the dropdown menu when trigger is inside a header or buttons container.

# 5.9.22
[Documentation](https://aui.atlassian.com/5.9.22/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.22)

## Fixed
* Using the keyboard to follow a dropdown item href link now works when the dropdown is first opened.
* Text for menu items in a collapsed sidebar now renders correctly on screen readers.

## Changed
* Improved documentation content and structure.

# 5.9.21
[Documentation](https://aui.atlassian.com/5.9.21/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.21)

# 5.9.20
[Documentation](https://aui.atlassian.com/5.9.20/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.20)

## Fixed
* Fixed broken package dependency which was causing installs to fail.
* Fixed issue where related aui-toggle property/attribute values did not match in Microsoft Edge.
* Fixed issue when initially-open Expander component could not be closed

## Changed
* Upgraded Skate to 0.13.17 to get fixes related to Microsoft Edge.
* Improved documentation content and structure.

# 5.9.19
[Documentation](https://aui.atlassian.com/5.9.19/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.19)

## Fixed
* Fixed the dialog arrow being positioned incorrectly when a deprecated Inline Dialog is instantiated with special CSS characters in the identifier.

# 5.9.18
[Documentation](https://aui.atlassian.com/5.9.18/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.18)

## Fixed
* Fixed Dropdown2 always changing the `href` attribute of its trigger to `'#'` even if the attribute was valid.
* Fixed inconsistencies between keyboard and mouse inputs when interacting with Dropdown2 menus with attached event handlers.
* Calling AJS.dim multiple times no longer resets its known list of aria-hidden elements.
* Fixed: aui-select component now correctly retains selected value on IE11
* Dropdown menus now support multiple triggers.
* Children of Inline dialog no longer lose event handlers during templating in non Chrome browsers
* Fixed: The aui-inline-dialog 'open' property and attribute now return consistent values
* Fixed Expander trigger text changing when an `aui-expander-expand` or `aui-expander-collapse` event is triggered and the expander is already expanded or collapsed.

# 5.9.17
[Documentation](https://aui.atlassian.com/5.9.17/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.17)

## Fixed
* Submenus in the sidebar can now be opened by calling the `show` method.
* Opening and closing a Dialog2 no longer overrides existing overflow styles

## Added
* Added a new event ("aui-responsive-menu-link-created") for when menus get moved into the responsive header.

# 5.9.16
[Documentation](https://aui.atlassian.com/5.9.16/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.16)

## Fixed
* Fixed a bug with Inline Dialog where an inline dialog with `responds-to="hover"` could fail to display correctly.
* Fixed an issue where a component anchored to another element (such as Dropdown 2) would duplicate alignment classes.
* Sidebar's submenus are initialised lazily, so adding submenus after calling AJS.sidebar(el) will work. This is the same behaviour as 5.7.x had.

# 5.9.15
[Documentation](https://aui.atlassian.com/5.9.15/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.15)

# 5.9.14
[Documentation](https://aui.atlassian.com/5.9.14/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.14)

# 5.9.13
[Documentation](https://aui.atlassian.com/5.9.13/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.13)

## Changed
* Upgraded Skate to 0.13.16. Skate no longer shares an element registry with older versions. This was causing problems with the lifecycles being initialised multiple times.

## Fixed
* Fixed an issue where flags had incorrect CSS for its borders

# 5.9.12
[Documentation](https://aui.atlassian.com/5.9.12/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.12)

## Fixed
* Back port from master of fix for event handler leakage in the AUI sidebar
* Back port from master of fix for form attribute handler for toggle button that wouldn't work in IE

# 5.9.11
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.10
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.9
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.8
[Documentation](https://aui.atlassian.com/5.9.8/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.8)

## Fixed
* Fixed alignment of the dialog arrow for Inline Dialog 2.

## Removed
* AUI was adding deprecated classes when `AJS.messages` was being called. This has since been removed. `AJS.messages.warning` will add `.aui-message-warning` only, not `.warning`.

# 5.9.7
[Documentation](https://aui.atlassian.com/5.9.7/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.7)

## Fixed
* Fixed an issue where setting the `hidden` property for an object in the JSON response for an async dropdown would not set the attribute on the item.
* Patched jquery.ui.mouse.js for AUI edge compatibility (backported https://bugs.jqueryui.com/ticket/7778 for v1.8.24)

# 5.9.6
[Documentation](https://aui.atlassian.com/5.9.6/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.6)

# 5.9.5
[Documentation](https://aui.atlassian.com/5.9.5/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.5)

## Fixed
* Fixed an issue where specifying a value for an `<aui-option>` inside an `<aui-select>` caused the option to not get properly selected.
* i18n fixed for `<aui-toggle>`, `<aui-select>` and `<aui-dropdown>`

## Added
* Event handlers attached to a sidebar can now be removed with the `off()` method.

# 5.9.4
[Documentation](https://aui.atlassian.com/5.9.4/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.4)

## Fixed
* Sidebar collapse/expand shortcut ([) no longer conflicts with browser back shortcut (CMD+[) on Mac OS X.

# 5.9.3
* [Documentation](https://aui.atlassian.com/5.9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.3)

## Fixed
* Events are now fired correctly when a progress indicator is updated.
* Tooltips using the `live` option now work correctly when initialised on empty collections.

# 5.9.2
* [Documentation](https://aui.atlassian.com/5.9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.2)

## Added
* Updated Soy templates to allow classes to be specified for form field elements directly using the `extraFieldClasses` parameter.

# 5.9.1
* [Documentation](https://aui.atlassian.com/5.9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.1)

## Added
* Tipsy tooltips can be destroyed with `$el.tipsy('destroy')`.

## Changed
* AUI datepicker internationalisation files have been changed from `.properties` files to `.js` files, for more modular consumption in plugins.
* An ID given to an <aui-select> is no longer moved to the <input> of the expanded DOM. Instead it behaves in line with the <aui-toggle>, adding the given ID with a suffix '-input' to the <input> of the expanded DOM.
* (perf) Removed fraction support for jQuery < 1.5
* (cosmetic) Min- and maxlength error messages for form validation are now more precise
* (cosmetic) Form validation message adapts when minlength == maxlength

## Fixed
* Dropdown, Dropdown 2, and Single Select no longer have a FOUC.
* Toggle Button's `checked` attribute now properly stays in sync with its state.
* Clicking a child of a dropdown item with the `aui-dropdown2-interactive` class doesn't hide the dropdown any more
* Tooltips are positioned correctly on `<svg>` elements, and their children.
* Expose named defines to make AUI work with AlmondJS
* Allow click event on a checkbox or radio button in a dropdown to be prevented

# 5.9.0
* [Documentation](https://aui.atlassian.com/5.9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.0)
* [Upgrade guide](https://aui.atlassian.com/5.9.0/docs/upgrade-guide.html)

## Added
* AUI is now available on the public npm registry. To install: `npm install @atlassian/aui`
* `can-create-values` boolean attribute for `<aui-select>` that allows the user to create new values.
* `no-empty-option` boolean attribute for `<aui-select>` that disallows empty choices.
* Experimental `<aui-dropdown-menu>` web component API for Dropdown 2.
* `<aui-toggle>` web component for toggle buttons ("on" and "off" states).
* `<aui-label>` web component that serves as a complement to specific AUI components, e.g., `<aui-toggle>`, to provide an accessible label.
* `<aui-badge>` web component API for Badges.
* Moved docs and test pages from AUI-ADG to AUI.

## Changed
* Refactored build from grunt to gulp.
* Experimental components no longer need a `require()` call to load them.
* `<aui-inline-dialog2>` renamed to `<aui-inline-dialog>`.
* `<aui-inline-dialog>` no longer needs a `require()` call to load it.
* `<aui-inline-dialog>`'s `persistent` attribute is now a boolean attribute.

## Deprecated
* All usage of named AMD modules. Use UMD source instead.
* `AJS()`, `AJS.filterBySearch()`, `AJS.include()`, `AJS.setVisible()`, `AJS.setCurrent()`, and `AJS.isVisible()` have been deprecated. Use jQuery or native alternatives instead.
* RESTful table helper methods (`AJS.triggerEvt()`, `AJS.bindEvt()`, and `AJS.triggerEvtForInst()`) have been deprecated and moved to RESTful table.
* Accessing the jQuery element on form validation fields via `field.$el` has been deprecated. Use `field.el` instead, which contains the native DOM element.
* `.aui-badge` class-based API for Badges has been deprecated. Use the `<aui-badge>` web component API instead.

## Fixed
* Several theming fixes and enhancements.
* Dropdowns are now closed when the browser window is resized.
* Sortable table arrow directions have been flipped to reflect the actual ordering.

## Removed
* `<aui-inline-dialog>`'s `isVisible()`, `show()`, and `hide()` have been removed. Use the `open` property instead.
* `<aui-inline-dialog>`'s `aui-layer-show` and `aui-layer-hide` events have been removed. Use `aui-show` and `aui-hide` instead.

# 5.8.16
* [Documentation](https://aui.atlassian.com/5.8.16/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.16)

# 5.8.17
* [Documentation](https://aui.atlassian.com/5.8.17/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.17)

## Changed
* Converted date picker to use properties files instead of hardcoded values.

## Fixed
* Fixed focus not being trapped inside a modal dialog for screen reader users.
* Tooltips are positioned correctly on `<svg>` elements, and their children.

## Added
* Tipsy tooltips can be destroyed with `$el.tipsy('destroy')`.
* Added web resources for jQuery UI Interactions, Widgets, and Position.

# 5.8.15
* [Documentation](https://aui.atlassian.com/5.8.15/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.15)

## Fixed
* Web components, e.g., Inline Dialog 2, are no longer subject to premature initialization in IE9 and IE10 that caused them to break.
* Dropdown 2 no longer has a resizing bug when open where it would stretch to the height of the browser when the browser was resized.
* Inline Dialog 2 has `display` set to `none` after hiding so that it no longer takes up space when hidden.
* Inline Dialog 2 no longer has a FOUC.
* Inline Dialog 2 now correctly fades when hidden.
* Inline Dialog 2 now checks if the show and/or hide event was cancelled before updating its trigger's `aria-expanded` attribute.
* Sidebar correctly unbinds keyboard handler for '[' shortcut, fixing a regression introduced in 5.8.14.
* Single Select's value can now be programatically set via the element's `value` property.
* Single Select now shows the display name of an option, rather than the text of the `value` attribute, fixing a regression introduced in 5.8.14.
* `aui-select` web-resource now declares all its dependencies.
* Sidebar's event examples in the documentation now correctly use a Sidebar instance, rather than the constructor.

# 5.8.14
* [Documentation](https://aui.atlassian.com/5.8.14/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.14)

## Fixed
* Sidebar now only enters touch mode for small screen touchable devices, i.e., width < 1024px.
* `value` property of `<aui-select>` now correctly returns the selected option's value rather than its label.
* Stopped jQuery Hotkeys throwing an error about `options.combo` being undefined.
* Sidebar no longer toggles when '[' is typed in an input field.

# 5.8.13
* [Documentation](https://aui.atlassian.com/5.8.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.13)

## Added
* Added `debounceImmediate()` (similar to `debounce()` but is immediately invoked).

## Changed
* Backport of jQuery UI feature detection performance improvement.
* "More" menu for responsive header is now lazily created to reduce work in onReady.
* Removed base64-encoded data URIs for icons from sidebar CSS. Changed to reference actual icon files now.

## Removed
* removed CSS vendor prefixes for `box-sizing` and `transition`

## Fixed
* `AJS.version` now contains the correct value rather than `${project.version}` (broken since 5.8.11).
* Various bug fixes for responsive header (reinsertion order, disappearing items, quickly resizing, keyboard navigation).
* Fixed a JavaScript error in the responsive header when secondary navigation was hidden.
* Fixed navigation soy template when there are collapsible children.
* Fixed keyboard navigation bug in Sidebar around collapsible children.
* There were some uncompiled ADG LESS files in the plugin.

# 5.8.12
* [Documentation](https://aui.atlassian.com/5.8.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.12)

## Changed
* Clicking on empty space in sidebar no longer collapses or expands it.
* Product logo images for the ADG header moved from data-uris in the LESS to external images.

## Fixed
* Improved robustness of Raphael detection.
* Sidebar in documents with short content now properly reflows when expanding window size in Chrome.
* Sidebar submenus can now be focused with the keyboard when collapsed.
* Keyboard shortcut ("[") for toggling the sidebar now works on German keyboard layouts.
* Fixed a sidebar JavaScript bug in IE9 (invalid classList lookup).
* Fixed a sidebar bug in IE9/10 where submenus would incorrectly show when hovered over.
* Fixed bug in JSON example in single-select docs.

# 5.8.11
* [Documentation](https://aui.atlassian.com/5.8.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.11)

## Changed
* dropdown2's `.aui-dropdown2-checkbox` and `.aui-dropdown2-radio` have had their `isDisabled()` method replaced with `isEnabled()`.

## Fixed
* Responsive header dropdown2s now properly open as submenus.
* Hidden dropdown2s no longer break alignment of menus on window resize.
* JavaScript error in single select documentation's async example.

# 5.8.10
* [Documentation](https://aui.atlassian.com/5.8.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.10)

## Fixed
* `eve.unbind()` is once again aliased as `eve.off()`.

# 5.8.9
* [Documentation](https://aui.atlassian.com/5.8.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.9)

## Fixed
* Proper fix for messages wrapping long unbroken strings.

# 5.8.8
* [Documentation](https://aui.atlassian.com/5.8.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.8)

## Fixed
* Form validation attributes changed at runtime are now respected on subsequent field validations.
* Messages now wrap long unbroken strings (break-word).
* Links (<a>) in the expanded sidebar now work again (broken since 5.8.0).
* Sidebar correctly sizes and positions itself at page load for short content heights.
* Markup examples for the Sidebar are now correct.

# 5.8.7
* [Documentation](https://aui.atlassian.com/5.8.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.7)

## Highlights
* Sidebar events now support `preventDefault()`.

# 5.8.6
* [Documentation](https://aui.atlassian.com/5.8.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.6)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.5
* [Documentation](https://aui.atlassian.com/5.8.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.5)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.4
* [Documentation](https://aui.atlassian.com/5.8.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.4)

# 5.8.3
* [Documentation](https://aui.atlassian.com/5.8.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.3)

# 5.8.2
* [Documentation](https://aui.atlassian.com/5.8.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.2)

## Upgrade Notes
* This release has some missing artifacts, please use 5.8.3 instead.

# 5.8.1
* [Documentation](https://aui.atlassian.com/5.8.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.1)

# 5.8.0
[Documentation](https://aui.atlassian.com/5.8.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.0)

## Highlights
* NEW COMPONENT: [AUI Single Select](https://aui.atlassian.com/5.8.0/single-select.html)
* Dropdown2 has been rewritten to improve accessibility. There is a new markup pattern, detailed in the [Dropdown 2 documentation](https://aui.atlassian.com/5.8.0/dropdown2.html). While the old markup pattern will work backwards compatibly, we encourage all developers to move to the new markup pattern to improve access for people using screen readers.

## Upgrade Notes
* aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
* If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js
* The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents)
* The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.
* Removed dependencies from AUI components to AUI soy templates (including responsive header).
* AUI Sandbox has been removed, in the future we will be uploading examples to jsbin.
* AUI Flag `persistent` option has been removed (deprecated in 5.7.7).  Update all usages of this option to use `close` instead (see https://aui.atlassian.com/5.8.0/flag.html).
* An AMD loader must be included in order to use aui-experimental.js now.

# 5.7.34
[Documentation](https://aui.atlassian.com/5.7.34/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.34)

## Highlights
* CSS deprecations have been added to the deprecation web-resource (`com.atlassian.auiplugin:aui-deprecation`)
* The docs now have the ability to push code examples to jsbin, codepen and jsfiddle

## Changed
* Converted date picker to use properties files instead of hardcoded values.

## Fixed
* Fixed focus not being trapped inside a modal dialog for screen reader users.

# 5.7.33
[Documentation](https://aui.atlassian.com/5.7.33/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.33)

# 5.7.32
[Documentation](https://aui.atlassian.com/5.7.32/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.32)

## Fixed
* Ensure Inline Dialog 2 has `display` set to `none` after hiding so that it doesn't take up space.

# 5.7.31
[Documentation](https://aui.atlassian.com/5.7.31/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.31)

## Fixed
* Sidebar now only enters touch mode for small screen touchable devices, i.e., width < 1024px.
* Sidebar no longer toggles when '[' is typed in an input field.

# 5.7.30
[Documentation](https://aui.atlassian.com/5.7.30/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.30)

# 5.7.29
[Documentation](https://aui.atlassian.com/5.7.29/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.29)

## Added
* Added `debounceImmediate()` (similar to `debounce()` but is immediately invoked).

## Fixed
* Fixed a JavaScript error in the responsive header when secondary navigation was hidden.

# 5.7.28
[Documentation](https://aui.atlassian.com/5.7.28/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.28)

## Changed
* Clicking on empty space in sidebar no longer collapses or expands it.
* Backport of jQuery UI feature detection performance improvement.
* "More" menu for responsive header is now lazily created to reduce work in onReady.

## Fixed
* Sidebar submenus can now be focused with the keyboard when collapsed.
* Various bug fixes for responsive header (reinsertion order, disappearing items, quickly resizing).
* There were some uncompiled ADG LESS files in the plugin.

# 5.7.27
[Documentation](https://aui.atlassian.com/5.7.27/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.27)

## Fixed
* Sidebar in documents with short content now properly reflows when expanding window size in Chrome.
* Improved performance of jQuery UI feature detection.

# 5.7.26
[Documentation](https://aui.atlassian.com/5.7.26/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.26)

## Fixed
* `eve.unbind()` is once again aliased as `eve.off()`.
* Improved robustness of Raphael detection.

## Changed
* Product logo images for the ADG header moved from data-uris in the LESS to external images.

# 5.7.25
[Documentation](https://aui.atlassian.com/5.7.25/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.25)

## Fixed
* Proper fix for messages wrapping long unbroken strings.

# 5.7.24
[Documentation](https://aui.atlassian.com/5.7.24/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.24)

## Fixed
* Form validation attributes changed at runtime are now respected on subsequent field validations.
* Messages now wrap long unbroken strings (break-word).

## Changed
* Updated Atlassian logo.

# 5.7.23
[Documentation](https://aui.atlassian.com/5.7.23/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.23)

# 5.7.22
[Documentation](https://aui.atlassian.com/5.7.22/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.22)

# 5.7.21
[Documentation](https://aui.atlassian.com/5.7.21/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.21)

* This is a broken release. Use 5.7.22 instead.

## Highlights
* Sidebar events now support `preventDefault()`.

# 5.7.20
[Documentation](https://aui.atlassian.com/5.7.20/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.20)

# 5.7.19
[Documentation](https://aui.atlassian.com/5.7.19/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.19)

# 5.7.18
[Documentation](https://aui.atlassian.com/5.7.18/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.18)

## Highlights
* Fix iconfont from previous version.

# 5.7.17
[Documentation](https://aui.atlassian.com/5.7.17/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.17)

## Upgrade notes
* This version contains some broken icons and should not be used.

# 5.7.16
[Documentation](https://aui.atlassian.com/5.7.16/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.16)

## Highlights
* The dist is now using version 0.11.4 of grunt-less-contrib, and version ~1.7.2 of the less compiler.

# 5.7.15
[Documentation](https://aui.atlassian.com/5.7.15/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.15)

# 5.7.14
[Documentation](https://aui.atlassian.com/5.7.14/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.14)

## Upgrade notes
* This version of the AUI plugin uses version 2.2.0 the `com.atlassian.lesscss` lesscss-maven-plugin, with less version 1.7.0. See AUI-3097 for details.

# 5.7.13
[Documentation](https://aui.atlassian.com/5.7.13/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.13)

# 5.7.12
[Documentation](https://aui.atlassian.com/5.7.12/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.12)

# 5.7.11
[Documentation](https://aui.atlassian.com/5.7.11/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.11)

# 5.7.10
[Documentation](https://aui.atlassian.com/5.7.10/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.10)

# 5.7.9
[Documentation](https://aui.atlassian.com/5.7.9/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.9)

# 5.7.8
[Documentation](https://aui.atlassian.com/5.7.8/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.8)

# 5.7.7
[Documentation](https://aui.atlassian.com/5.7.7/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.7)

## Highlights
* Fixed persistent flags to be dismissable. See [AUI-2893](https://ecosystem.atlassian.net/browse/AUI-2893)

## Upgrade Notes
**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Flags created with the previous 5.7.1 API will work exactly as they did previously in 5.7.7. Upgrading from these versions
is **non breaking** (existing calls to flag will behave exactly as they did). However, the "persistent" has been deprecated,
and the "close" option introduced instead.
* Flags with "persistent: true" should become "close: never"
* Flags with "persistent: false" should become "close: manual" (close: "manual" is the default, so you do not need to explicitly set this)

**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Upgrading from 5.7.4 **will change** behaviour of flags with "persistent: false" set. In 5.7.4, flags automatically faded
from view. They now require a user to dismiss them (they have close: "manual" behaviour)
* Flags with "persistent: true" should become "close: never". Flags with "persistent: true" maintain the same behaviour.
* Flags with "persistent: false" no longer fade after five seconds. If you wish them to, they should become "close: auto". If you do do not wish them to, they should become "close: manual"

#5.7.6
This is a broken release containing no bugfixes and should not be used. Please use the latest 5.7.x release instead

#5.7.5
[Documentation](https://aui.atlassian.com/5.7.5/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.5)

#5.7.4
[Documentation](https://aui.atlassian.com/5.7.4/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.4)

## Highlights
* Flags auto-close was ported over to this release from master.

#5.7.3
[Documentation](https://aui.atlassian.com/5.7.3/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.3)

## Highlights
* New Confluence Icon Fonts
Note: normally we would do this in a minor version. However to avoid upgrade friction for Confluence we've determined it is acceptable to put these in a patch release.

#5.7.2
[Documentation](https://aui.atlassian.com/5.7.2/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.2)

#5.7.1
[Documentation](https://aui.atlassian.com/5.7.1/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.1)

#5.7.0
[Documentation](https://aui.atlassian.com/5.7.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.0)

## Highlights
* Buttons have become flat and have had gradients removed
* Inset shadows have been removed from fields in forms
* Form notification is available as a new API (show tooltip messages on fields)
* Flag messages are available as a new API
* Inline Dialog 2 is available as a new API
* System notifications have been changed (no longer electric Charlie)
* The following components have no global variable and are only exposed via AMD modules:
  * Flags
  * Form Notification
  * Form Validation
  * Inline Dialog 2
* [Animated examples](https://developer.atlassian.com/display/AUI/AUI+5.7.0+Release+Notes) are available.

## Upgrade Notes
* If you have been using Form Validation, please change your data attributes
  * from: `<input data-aui-validate-... />`
  * to: `<input data-aui-validation-... />`
* If you are using Inline Dialog 2, Flags, Form Notification or Form Validation, you will need an AMD loader to initialise their behaviour on pages:
  * `require(['aui/inline-dialog2']); // Initialises all Inline Dialog 2s`
  * `require(['aui/flag']); // Initialises all flags`
  * `require(['aui/form-notification']); // Initialises all form notifications`
  * `require(['aui/form-validation', 'form-provided-validators']); // Loads and initialises form validations (and notifications) API. Also loads all AUI-provided form validators`

For all release notes older than 5.7.0, check [DAC].


[WRM]: https://bitbucket.org/atlassian/atlassian-plugins-webresource/
[DAC]: https://developer.atlassian.com/display/AUI/AUI+release+notes
