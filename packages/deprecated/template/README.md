# AUI Template

A utility function for rendering HTML using string interpolation.
Extracted from the [AUI library][aui].

[aui]: https://aui.atlassian.com/
