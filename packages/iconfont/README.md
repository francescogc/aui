# ADG Server Icon font

Atlassian ADG Server icons in a handy font format,
useful for building user interfaces in Atlassian products and services.

## Usage

This icon font is included in [the AUI library](https://aui.atlassian.com/latest/docs/icons.html).

## Contributing

Read the [HOWTO.md] file for instructions on changing and re-generating the icon font.

## License

The ADG Server icon font is an Atlassian Developer Asset and is released under the [Atlassian Developer Terms license](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/).
