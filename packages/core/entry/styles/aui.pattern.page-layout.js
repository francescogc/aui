import './aui.page.reset';
import './aui.page.typography';
import './aui.page.links';
import '@atlassian/aui/src/less/aui-page-layout.less';
import '@atlassian/brand-logos/dist/aui-legacy/aui-footer.css'; // todo: remove in AUI 9.
