import './aui.page.reset';
import './aui.page.typography';
import './aui.pattern.basic';
import './aui.pattern.button'; // because tables override whether they fit inside a cell
import './aui.pattern.avatar'; // because 'table-list' pattern changes opacity of them
import './aui.pattern.icon'; // because 'table-list' pattern changes opacity of them
import '@atlassian/aui/src/less/tables.less';
