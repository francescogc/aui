import './styles/aui.page.iconography';
import './styles/aui.pattern.icon';
import './styles/aui.pattern.forms';
import './aui.component.tooltip';
import '@atlassian/aui/src/less/form-notification.less';
import '@atlassian/aui/src/js/aui/form-notification.js';
export {};
