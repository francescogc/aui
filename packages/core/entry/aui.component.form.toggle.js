import './styles/aui.pattern.forms';
import './aui.component.spinner'; // for the async variant
import '@atlassian/aui/src/less/aui-toggle.less';
export { default as ToggleEl } from '@atlassian/aui/src/js/aui/toggle';
