import './styles/aui.pattern.banner';
import './styles/aui.pattern.messages'; // for header interop
import '@atlassian/aui/src/less/aui-banner-message-header-interop.less';
export { default as banner } from '@atlassian/aui/src/js/aui/banner';
