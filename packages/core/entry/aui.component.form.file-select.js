import './styles/aui.pattern.forms';
import 'fancy-file-input/dist/fancy-file-input.css';
import '@atlassian/aui/src/less/adg-fancy-file-input.less';
export { default as FancyFileInput } from '@atlassian/aui/src/js/aui/fancy-file-input';
