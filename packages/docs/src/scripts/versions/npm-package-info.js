const registryUri = 'https://registry.npmjs.org';

const escapePackageName = name => name.replace('/', encodeURIComponent('/'));
const corsServiceUrl = 'https://cors-anywhere.herokuapp.com';

export default function getNpmPackageInfo(pkgName) {
    const url = `${registryUri}/${escapePackageName(pkgName)}`;
    const fullUrl = `${corsServiceUrl}/${url}`;

    return fetch(fullUrl).then(response => response.json());
}
