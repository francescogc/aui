import { getAllVersions, getStableVersions, getLatestStableVersions  } from './versions-helper';

const buildLatestStableVersionsHtml = (versions) => {
    let result = '';

    versions
        .reverse()
        .forEach((version, i) => {

            let content = version;

            if (i === 0) {
                content += '<span class="aui-lozenge aui-lozenge-subtle aui-lozenge-success">Latest</span>';
            }

            result += `<aui-item-link href="/aui/${version}">${content}</aui-item-link>`;
        });

    return result;
};

function updateVersionsOnDropdown(dropdownSection, versions) {
    const stableVersions = getLatestStableVersions(versions);
    const latestStableVersionHtml = buildLatestStableVersionsHtml(stableVersions);

    dropdownSection.innerHTML = latestStableVersionHtml;
}

window.document.addEventListener('DOMContentLoaded', () => {
    getAllVersions().then(allVersions => {
        const versions = getStableVersions(allVersions);

        const dropdownContainer = document.getElementById('stable-versions-dropdown');

        if (dropdownContainer) {
            updateVersionsOnDropdown(dropdownContainer, versions);
        }
    });
});
