---
component: Typography
analytics:
  pageCategory: component
  component: typography
design: https://design.atlassian.com/latest/product/foundations/typography/
status:
  api: general
  core: true
  wrm: com.atlassian.auiplugin:aui-page-typography
  experimentalSince: 3.6.3
  generalSince: 5.1.0
---

<h2>Summary</h2>

<p>Typography is a CSS component for use with Page and Layout. It adds basic styling for common typographic elements including base text, headings and lists. It is primarily concerned with setting font size, typeface and line height; and providing spacing
    around common elements. It sets a very simple baseline.</p>
<p>Typography applies directly to HTML element selectors, to ensure the styles are applied lightly enough that they can be overridden when required.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>

<h3>Headings and heading level semantics</h3>

<p>
    The <code>h1</code> through <code>h6</code> elements in AUI have been given default font sizes and weights that
    provide a legible hierarchy when used in page content.
</p>

<article class="aui-flatpack-example">
    <h1>Main title heading</h1>

    <p>
        Atlassian's design guidelines call this <dfn>h700</dfn>.
        <br />
        The <code>h1</code> is optimised for "main title" headings &mdash; the heading for the main content of a page.
        It should typically appear once per page.
    </p>

    <h2>Key functionality headings</h2>

    <p>
        Atlassian's design guidelines call this <dfn>h600</dfn>.
        <br/>
        The <code>h2</code> should be used for headings that identify key functionality on a page.
    </p>

    <h3>Sub-section and field group headings</h3>

    <p>
        Atlassian's design guidelines call this <dfn>h500</dfn>.
        <br/>
        Use <code>h3</code> if you have multiple logical groupings of content on the page.
    </p>

    <h4>Deep headings</h4>

    <p>
        Atlassian's design guidelines call this <dfn>h400</dfn>.
        <br />
        By the time you get to using <code>h4</code>, you will no doubt be deeply nested in the page's content
        and hierarchy. Use these when you need to highlight important pieces of information.
    </p>

    <section>
        <h5>List headings</h5>

        <ul>
            <li>The <code>h5</code> element is intended for use above groups of items.</li>
            <li>Use it within page content above ordered, unordered, or structured lists of data.</li>
            <li>Atlassian's design guidelines call this <dfn>h300</dfn>.</li>
        </ul>
    </section>

    <h6>Low-level headings</h6>

    <p>
        Atlassian's design guidelines specify both <dfn>h100</dfn> and <dfn>h200</dfn> as "low-level headings".
        <br/>
        You will only tend to see <code>h6</code> in use inside other UI widgets, such as in the vertical
        navigation pattern on a page, or dropdown item group headings. This heading level should be used sparingly
        in user-level content.
    </p>
</article>

<p>
    In addition to their basic font sizes and visual treatments, the margins between each heading level element
    have been tweaked to improve rendering, either when added inside other box containers, or when they occur
    immediately after each-other in the DOM.
</p>

<article class="aui-flatpack-example">
    <div class="aui-group">
        <div class="aui-item">
            <h1>Heading margins!</h1>
            <h2>A tale of heading levels</h2>
            <p>A top-level heading inside a group item has no top margin.</p>
        </div>
        <div class="aui-item">
            <h3>A sub-heading appears!</h3>
            <p>Even though it is in a separate grouping, it begins at a similar height to other headings.</p>
        </div>
        <div class="aui-item">
            <table>
                <tr>
                    <td>
                        <h2>Headings inside tables</h2>
                        <p>Let's face it: this isn't a great idea. But, the margins collapse, just in case.</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</article>

<h3>Content-level semantics</h3>

<h4>Quoted content</h4>

<p>
    Use <code>blockquote</code> when a quotation that is typically longer than a few lines.
    It will be indented to set it apart from surrounding text.
    Pair this with <code>cite</code> so readers will know who to attribute the quote to!
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <blockquote>
            <p>All that is gold does not glitter, not all those who wander are lost; The old that is strong does not wither, deep roots are not reached by the frost.</p>
            <p> From the ashes a fire shall be woken, a light from the shadows shall spring; Renewed shall be blade that was broken, the crownless again shall be king.</p><cite>J.R.R. Tolkien, The Fellowship of the Ring</cite>
        </blockquote>
    </noscript>
</aui-docs-example>

<h4>Code content</h4>

<p>These examples get a bit meta!</p>
<p>
    Use the <code>code</code> tag to denote inline code examples. Use <code>var</code> when referring to specific
    mathematical or programming variables.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <p>
            For a given number of people <var>N</var>, the likelihood
            they will use the appropriate HTML element for their code can be expressed as <code>N = RC / e</code>,
            where <var>R</var> is the percentage who have read this sentence, <var>C</var> is the likelihood
            they care about semantics, and <var>e</var> is the number of possible other elements they could use.
        </p>
    </noscript>
</aui-docs-example>

<p>Use <code>pre</code> when outputting multiple lines of related code.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
    <pre>
        <code>
            &lt;div class="foo"&gt;
                &lt;h1&gt;Example code snippet&lt;/h1&gt;
                &lt;p&gt;
                    Don't forget to properly HTML escape your content!
                    Also, remember that the "pre" tag treats whitespace significantly.
                &lt;/p&gt;
            &lt;/div&gt;
        </code>
    </pre>
    </noscript>
</aui-docs-example>
