---
component: Sortable tables
analytics:
  pageCategory: component
  component: sortable-table
design: https://design.atlassian.com/latest/product/components/tables/
status:
  api: general
  core: false
  wrm: com.atlassian.auiplugin:aui-table-sortable
  amd: false
  experimentalSince: 5.1
  generalSince: 5.8
---
<h2>Summary</h2>

<p>Sortable Tables adds on to the core Tables component, allowing the table to be sorted by the header row. Some
    custom sorting options are available.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>
<table id="delayedSortedTable" class="aui aui-table-sortable">
    <thead>
    <tr>
        <th class="aui-table-column-issue-key">Issue key</th>
        <th>Name</th>
        <th>Age</th>
        <th class="aui-table-column-unsortable">Description</th>
    <tr>
    </thead>
    <tbody>
    <tr>
        <td>TEST-12</td>
        <td>Cannot sort tables</td>
        <td>2</td>
        <td>Table sorting should be allowed</td>
    </tr>
    <tr>
        <td>WIN-87</td>
        <td>Issue Page doesn't load in IE</td>
        <td>7</td>
        <td>When loading issue page on IE it doesn't show</td>
    </tr>
    <tr>
        <td>DRINK-7</td>
        <td>Vending Machine is empty</td>
        <td>1</td>
        <td>Blocker</td>
    </tr>
    </tbody>
</table>

<h2>Code</h2>

<h3>Setup</h3>

<p>
    To make a table sortable, you must add the <code>aui</code> and <code>aui-table-sortable</code>
    CSS classes to the table and also define the &lt;thead&gt; of the
    table.
</p>

<p>
    Sortable Tables are determined at page load, so if a table is added to the DOM after page load
    then it must be declared as sortable in your own code.
</p>

<h3>Default sorting behaviour</h3>

<p>
    Sortable tables attempt to automatically detect the best sorting method for a given column.
    The sorting method can be explicitly set by adding a CSS class to a column's <code>th</code> element.
    Most of the sorting methods are provided by
    <a href="https://mottie.github.io/tablesorter/docs/">the jQuery tablesorter plugin</a>.
    Some custom sorting methods provided by AUI include:
</p>
<ul>
    <li><code>aui-table-column-issue-key</code>:
        Sorts JIRA issue keys so that they are in the correct ordering by project and
        then by issue id.
    </li>
    <li><code>aui-table-column-unsortable</code>:
        Disallows any sorting of this column.
    </li>
</ul>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <table class="aui aui-table-sortable">
            <thead>
                <tr>
                    <th class="aui-table-column-issue-key">Issue key</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th class="aui-table-column-unsortable">Description</th>
                <tr>
            </thead>
            <tbody>
                <tr>
                    <td>TEST-12</td>
                    <td>Cannot sort tables</td>
                    <td>2</td>
                    <td>Table sorting should be allowed</td>
                </tr>
                <tr>
                    <td>WIN-87</td>
                    <td>Issue Page doesn't load in IE</td>
                    <td>7</td>
                    <td>When loading issue page on IE it doesn't show</td>
                </tr>
                <tr>
                    <td>DRINK-7</td>
                    <td>Vending Machine is empty</td>
                    <td>1</td>
                    <td>Blocker</td>
                </tr>
            </tbody>
        </table>
    </noscript>
</aui-docs-example>

<h3>Custom sorting behaviour</h3>

<p>
    Through the <a href="https://mottie.github.io/tablesorter/docs/example-parsers.html">jQuery tablesorter plugin API</a>, it is possible to define your own sorting
    behaviour. The following is an example of how you might order a "Priority" column using a non-alphabetical
    sort order.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <table id="custom-parser-example" class="aui aui-table-sortable">
            <thead>
            <tr>
                <th class="aui-table-column-issue-key">Issue key</th>
                <th class="sorter-priority">Priority</th>
                <th>Last updated</th>
                <th>Summary</th>
            <tr>
            </thead>
            <tbody>
            <tr>
                <td>TEST-12</td>
                <td>High</td>
                <td><time datetime="2017-04-20T14:00Z0">April 20, 2017</time></td>
                <td>Table sorting should be allowed</td>
            </tr>
            <tr>
                <td>WIN-87</td>
                <td>Blocker</td>
                <td><time datetime="2018-04-23T11:30Z0">April 23, 2018</time></td>
                <td>Issue Page doesn't load in IE</td>
            </tr>
            <tr>
                <td>DRINK-7</td>
                <td>Low</td>
                <td><time datetime="2018-04-01T17:30Z0">April 1, 2018</time></td>
                <td>Vending Machine is empty</td>
            </tr>
            <tr>
                <td>TEST-7</td>
                <td>Medium</td>
                <td><time datetime="2018-01-11T09:30Z0">January 11, 2018</time></td>
                <td>My hovercraft is full of eels</td>
            </tr>
            </tbody>

        </table>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        (function() {
            var priorityMap = {
                'high': 3,
                'blocker': 5,
                'low': 1,
                'medium': 2
            };

            AJS.$.tablesorter.addParser({
                id: 'priority',
                is: function(nodeValue, table, cell) {
                    return false;
                },
                format: function(nodeValue, table, cell) {
                    var raw = (nodeValue || '').toLowerCase().trim();
                    return priorityMap[raw] || -1;
                },
                type: 'numeric'
            });

            // Since we added a new sorter, but the table was added at page load,
            // we'll need to kick off the sorting again.
            AJS.$('#custom-parser-example').trigger('sort');
        }());
    </noscript>
</aui-docs-example>

<h3>JavaScript API</h3>

<p>
    To set a table as sortable, you must pass a jQuery object for the table into the
    <code>AJS.tablessortable.setTableSortable</code> function.
</p>
<noscript is="aui-docs-code" type="text/js">
    AJS.tablessortable.setTableSortable(AJS.$("#delayedSortedTable"));
</noscript>

<p>
    Assigning a table as sortable multiple times can result in undefined behavior. For example, if a sortable table is
    present on page load and then you assign it as sortable via the <code>AJS.tablessortable.setTableSortable</code>
    function, it is unlikely to work as expected.
</p>
