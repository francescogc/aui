---
component: Dropdown2
analytics:
  pageCategory: component
  component: dropdown2
status:
  api: deprecated
  core: false
  wrm: com.atlassian.auiplugin:aui-dropdown2
  amd: false
  experimentalSince: 4.0
  generalSince: 5.1
---
<div class="aui-message aui-message-warning">
    The <strong>Dropdown2 markup pattern</strong> has been deprecated in favor of using the dropdown menu web component.
    <ul class="aui-nav-actions-list">
        <li><a href="{{rootPath}}docs/dropdown.html">View Dropdown component documentation</a></li>
    </ul>
</div>

<h2>Summary</h2>
<p>Dropdown2 creates a dropdown menu, with optional sections, headings, icons, checkbox items, radio group items and disabled items. Submenus are supported but should be avoided where possible for usability reasons.</p>
<p>Dropdown2 is specifically created to fill the use case of a dropdown menu - extensions and overrides are not supported.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>
<article class="aui-flatpack-example">
    <!-- Simple Dropdown - trigger -->
    <p>
        <a href="#dwarfers-1" aria-owns="dwarfers-1" aria-haspopup="true" class="aui-button aui-dropdown2-trigger aui-style-default">Shipmates</a>
    </p>

    <!-- Simple Dropdown - dropdown -->
    <div id="dwarfers-1" class="aui-dropdown2 aui-style-default">
        <ul class="aui-list-truncate">
            <li><a href="https://example.com/">Lister</a></li>
            <li><a href="https://example.com/">Rimmer</a></li>
            <li><a href="https://example.com/">Cat</a></li>
            <li><a href="https://example.com/">Kryten</a></li>
        </ul>
    </div>

    <!-- Dropdown with sections - trigger -->
    <p>
        <a href="#red-dwarf-ships-1" aria-owns="red-dwarf-ships-1" aria-haspopup="true" class="aui-button aui-dropdown2-trigger aui-style-default">The Crimson Short One</a>
    </p>

    <!-- Dropdown with sections - dropdown -->
    <div id="red-dwarf-ships-1" class="aui-dropdown2 aui-style-default">
        <div class="aui-dropdown2-section">
            <ul>
                <li><a href="#foo">Red Dwarf</a></li>
                <li><a href="#foo">Low Red Dwarf</a></li>
                <li><a href="#foo">High Red Dwarf</a></li>
                <li><a href="#foo">Nanobot Red Dwarf</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <div class="aui-dropdown2-heading">
                <strong>Support Ships</strong>
            </div>
            <ul>
                <li><a href="#foo">Starbug</a></li>
                <li><a href="#foo">Blue Midget</a></li>
                <li><a href="#foo">White Giant</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <div class="aui-dropdown2-heading">
                <strong>Other Ships</strong>
            </div>
            <ul>
                <li><a href="#foo">Wildfire</a></li>
                <li><a href="#foo">SSS Esperanto</a></li>
                <li><a href="#foo">Nova 5</a></li>
            </ul>
        </div>
    </div>
    <!-- .aui-dropdown2 -->

    <!-- Dropdown with everything - Trigger -->
    <p>
        <button aria-owns="dropdown2-all-in" aria-haspopup="true" class="aui-button aui-dropdown2-trigger">
            Dropdown with everything
        </button>
    </p>

    <!-- Dropdown with everything - Dropdown -->
    <div id="dropdown2-all-in" class="aui-dropdown2 aui-style-default">
        <div class="aui-dropdown2-section">
            <ul>
                <li><a href="#">Edit Issue</a></li>
                <li><a class="disabled" title="You don't have permission to edit this issue.">Delete</a></li>
                <li><a href="#">Comment</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <div class="aui-dropdown2-heading">
                <strong>Transitions</strong>
            </div>
            <ul>
                <li><a href="#">Start Progress</a></li>
                <li><a href="#">Close Issue</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <ul>
                <li><a class="aui-dropdown2-radio interactive checked">Option 1</a></li>
                <li><a class="aui-dropdown2-radio interactive">Option 2</a></li>
                <li><a class="aui-dropdown2-radio interactive">Option 3</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <ul>
                <li><a class="aui-dropdown2-checkbox interactive">Check Spelling While Typing</a></li>
                <li><a class="aui-dropdown2-checkbox interactive checked disabled">Enable Gravity</a></li>
            </ul>
        </div>
        <div class="aui-dropdown2-section">
            <ul class="aui-list-truncate">
                <li>
                    <a href="https://example.com/" class="aui-icon-container">
                        <span class="aui-icon aui-icon-small aui-iconfont-view"></span>
                        Icon span pattern
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <script>
        // No javascript required for simple dropdowns.
        // If required you can programmatically invoke
        // Dropdown2 using the aui-button-invoke event:
        // AJS.$("#myDropdown2Trigger").trigger("aui-button-invoke");
    </script>
</article>

<h2>Code</h2>

<h3>Simple dropdown</h3>
<p>There are two parts to the dropdown: the trigger and the actual dropdown. The dropdown can initially be included anywhere in the DOM – it will be appended to the body element when invoked (avoids stacking problems); and re-appended after the trigger when dismissed.</p>
<p>Triggers are paired with their dropdown using the aria-owns attribute, which must correspond to the ID of the dropdown.</p>

<p>Trigger:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#dwarfers" aria-owns="dwarfers" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="dwarfers" class="aui-style-default aui-dropdown2">
            <ul class="aui-list-truncate">
                <li><a href="#">Menu item</a></li>
                <li><a href="#">Menu item</a></li>
                <li><a href="#">Menu item</a></li>
                <li><a href="#">Menu item</a></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>
<p>The <code>aria-owns</code> attribute must match the dropdown ID. The href should be either a hash link targeting the dropdown ID, or a URL to a relevant page. Both <code>aria-haspopup="true"</code> and the class <code>aui-dropdown2-trigger</code> must be present or the dropdown will not work.</p>

<h2>Options</h2>
<p>All Dropdown2 options are set via the markup, no JavaScript is required unless you wish to make use of events, or programmatically show and hide a dropdown.</p>

<h3>Dropdown2 trigger without an arrow</h3>
<p>If you need a dropdown trigger without an arrow you can specify an extra class:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"></button>
    </noscript>
</aui-docs-example>

<h3>Dropdown with groups and headings</h3>
<p>Dropdowns can have grouped items (visually this means they have a line between them), with an optional heading:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#red-dwarf-ships-trigger" aria-owns="red-dwarf-ships" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="red-dwarf-ships" class="aui-style-default aui-dropdown2">
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a href="#foo">Red Dwarf</a></li>
                    <li><a href="#foo">Low Red Dwarf</a></li>
                    <li><a href="#foo">High Red Dwarf</a></li>
                    <li><a href="#foo">Nanobot Red Dwarf</a></li>
                </ul>
            </div>
            <div class="aui-dropdown2-section">
                <div class="aui-dropdown2-heading">
                    <strong>Support Ships</strong>
                </div>
                <ul>
                    <li><a href="#foo">Starbug</a></li>
                    <li><a href="#foo">Blue Midget</a></li>
                    <li><a href="#foo">White Giant</a></li>
                </ul>
            </div>
            <div class="aui-dropdown2-section">
                <div class="aui-dropdown2-heading">
                    <strong>Other Ships</strong>
                </div>
                <ul>
                    <li><a href="#foo">Wildfire</a></li>
                    <li><a href="#foo">SSS Esperanto</a></li>
                    <li><a href="#foo">Nova 5</a></li>
                </ul>
            </div>
        </div>
    </noscript>
</aui-docs-example>

<h3>Dropdown width and truncation</h3>
<p>The minimum and maximum widths of a dropdown container can be controlled with CSS:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/css">
        #custom-width-example {
            min-width: 240px;
            max-width: 480px;
        }
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#custom-width-example-trigger" aria-owns="custom-width-example" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="custom-width-example" class="aui-style-default aui-dropdown2">
            <ul class="aui-list-truncate">
                <li><a href="#short">Short label</a></li>
                <li><a href="#long">Especially long label that contains enough text to feed a walrus and also to cause this list item to flow over multiple lines</a></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<h3>Dropdown with icons</h3>
<p>An icon can be included in a Dropdown item by adding <code>class="aui-icon-container"</code> to the item's <code>&lt;a&gt;</code> element. The icon can be set with a <code>&lt;span&gt;</code> (default pattern for icons), <code>&lt;img&gt;</code> or as a background image with CSS:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#dropdown2-icons-wrap-trigger" aria-owns="dropdown2-icons-wrap" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="dropdown2-icons-wrap" class="aui-style-default aui-dropdown2">
            <ul>
                <li>
                    <a href="https://example.com/" class="aui-icon-container">
                        <span class="icon icon-example">Alt text</span> Icon span pattern
                    </a>
                </li>
                <li>
                    <a href="https://example.com/" class="aui-icon-container">
                        <img src="images/icon-test-16x16.png" alt="Alt text"> Icon as IMG element
                    </a>
                </li>
                <li>
                    <a href="https://example.com/" class="aui-icon-container icon-example">Icon as background image</a>
                </li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<h3>Disabled dropdowns</h3>
<p>You can disable an entire dropdown by adding <code>aria-disabled="true"</code> to the trigger. You can enable it by setting <code>aria-disabled="false"</code>.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <a href="URI" aria-owns="ID" aria-haspopup="true" aria-disabled="true" class="aui-button aui-style-default aui-dropdown2-trigger">Make me a sandwich</a>
    </noscript>
</aui-docs-example>

<h3>Disabled dropdown items</h3>
<p>You can apply a disabled style to specific items within a dropdown by adding <code>class="disabled"</code> to the item's A element. Ideally, the A element's href attribute should also be removed, although this is not strictly required. Note that this is a style only; you will need to disable any functionality you have attached to that item.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#dropdown2-disabled-items-trigger" aria-owns="dropdown2-disabled-items" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="dropdown2-disabled-items" class="aui-style-default aui-dropdown2">
            <ul>
                <li><a href="#">Attach file</a></li>
                <li><a href="#">Comment</a></li>
                <li><a class="disabled" title="You don't have permission to edit this issue.">Edit issue</a></li>
                <li><a href="#">Watch issue</a></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<h3>Hidden items</h3>
<p>While not a recommended pattern, it is sometimes required to have a hidden item still present in a dropdown - to do this, add <code>class="hidden"</code> to the LI element. To ensure this does not break keyboard navigation <strong>you must also disable the trigger within the item</strong>.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#dropdown2-hidden-items-trigger" aria-owns="dropdown2-hidden-items" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="dropdown2-hidden-items" class="aui-style-default aui-dropdown2">
            <ul>
                <li><a href="https://example.com/">Visible item</a></li>
                <li class="hidden"><a class="disabled" aria-disabled="true" href="https://example.com/">Invisible item</a></li>
                <li><a href="https://example.com/">Visible item</a></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<p>On load, Dropdown2 will automatically disable A elements inside LI.hidden elements (that is it will add <code>aria-disabled="true"</code> and the disabled class to the A). If you need to hide an item after the dropdown has been opened you will need to set these attributes directly.</p>
<p>To reinstate the item, your code will have to do all of the following:</p>
<ol>
    <li>Set the&nbsp;<code>aria-disabled</code>&nbsp;attribute to "false" on the A</li>
    <li>Remove the&nbsp;<code>disabled</code>&nbsp;class from the A</li>
    <li>Remove the&nbsp;<code>hidden</code>&nbsp;class from the LI</li>
</ol>
<p>While this is acknowledged as being a bit verbose, reinstating items after page load is not considered a common use case at this stage.</p>

<h3>Checkboxes, Radio Groups, Interactive Items</h3>
<p>Checkboxes and radios can be added to a dropdown menu by using <code>class="aui-dropdown2-checkbox"</code> and <code>class="aui-dropdown2-radio"</code> on the A elements. The checked state of a checkbox or radio is indicated with <code>class="checked"</code>, and the unchecked state is indicated by the absence of this class. Radios are grouped by their containing UL element.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#dropdown2-checkbox-radio-interactive-trigger" aria-owns="dropdown2-checkbox-radio-interactive" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">
            Dropdown trigger
        </a>

        <!-- Dropdown -->
        <div id="dropdown2-checkbox-radio-interactive" class="aui-dropdown2 aui-style-default">
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a class="aui-dropdown2-radio interactive checked">Option 1</a></li>
                    <li><a class="aui-dropdown2-radio interactive">Option 2</a></li>
                    <li><a class="aui-dropdown2-radio interactive">Option 3</a></li>
                </ul>
            </div>
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a id="spellcheck" class="aui-dropdown2-checkbox interactive">Check spelling while typing</a></li>
                    <li><a class="aui-dropdown2-checkbox interactive checked disabled">Enable gravity</a></li>
                </ul>
            </div>
        </div>
    </noscript>
</aui-docs-example>
<p>Adding <code>class="interactive"</code> to an A element will prevent the dropdown container from hiding when an item is clicked. This is useful for checkboxes and radios so that the user can see the effect of their actions, but it can also be applied separately where needed.</p>

<h3>Dropdown menu groups/toolbars</h3>
<p>Related dropdown menus can be grouped together in a toolbar by adding <code>class="aui-dropdown2-trigger-group"</code> to a common ancestor of each dropdown trigger.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-header">
            <ul class="aui-nav aui-dropdown2-trigger-group">
                <li><a href="#foo" class="aui-dropdown2-trigger">Foo</a></li>
                <li><a href="#bar" class="aui-dropdown2-trigger">Bar</a></li>
                <li><a href="#sin" class="aui-dropdown2-trigger">Sin</a></li>
            </ul>
        </div>
    </noscript>
</aui-docs-example>

<p>This adds extra interactions such as keyboard navigation between menus (left/right arrow keys); and grouped dropdowns opening by mouse hover after the first one is activated with a click.</p>

<h3>Control left/right alignment of dropdown by setting a custom container</h3>
<p>Normally dropdowns align to the left or right based on the trigger's proximity to the edge of the viewport. That is, if it's too close to the right, it will extend left instead. However in some scenarios (eg. fixed-width content designs, or dialogs) you may want a dropdown to decide alignment based on something other than the viewport.</p>
<p>To do that, set the optional <code>data-aui-alignment-container</code> attribute on the dropdown, with a jQuery selector (usually an ID or class):</p>
<noscript is="aui-docs-code" type="text/html">
    <a href="URI" aria-owns="ID" aria-haspopup="true" class="aui-dropdown2-trigger" data-aui-alignment-container="JQUERYSELECTOR">Text</a>
</noscript>

<p>When the dropdown opens, it will compare its position to the closest instance of that jQuery selector.</p>
<p>For example, to contain dropdowns within a div with ID 'container':</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/css">
        #container {
            border: 1px solid black;
            margin-left: 100px;
            width: 300px;
        }

        #container a {
            left: 200px;
        }
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <div id="container">
            <a href="#" aria-owns="dropdownid" aria-haspopup="true" class="aui-dropdown2-trigger">Text</a>
        </div>

        <!-- Dropdown -->
        <div id="dropdownid" class="aui-dropdown2 aui-style-default" data-aui-alignment-container="#container"><a href="#">Test</a></div>
    </noscript>
</aui-docs-example>

<h3>Controlling where the dropdown is hidden</h3>
<p>From 5.0, by default a dropdown will be returned to its original location when hidden. You can also specify a custom "hide" location by adding a <code>data-dropdown2-hide-location</code> attribute to the dropdown trigger. The attribute should specify the ID of the element where you want to hide your dropdown.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#" aria-owns="hidedemo" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger" data-dropdown2-hide-location="hidelocation">Dropdown trigger</a>

        <!-- Dropdown -->
        <div id="hidedemo" class="aui-style-default aui-dropdown2">
            <ul><li><a href="#">Dropdown item</a></li></ul>
        </div>

        <!-- The dropdown gets hidden here -->
        <div id="hidelocation"></div>
    </noscript>
</aui-docs-example>

<h3>Programmatically showing or hiding a dropdown</h3>
<p>To programmatically open a dropdown menu, dispatch an "aui-button-invoke" jQuery event on the dropdown trigger element:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/js">
        AJS.$("#programmatic-dropdown-trigger").trigger("aui-button-invoke");
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#" id="programmatic-dropdown-trigger" aria-owns="programmatic-dropdown" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">
            Dropdown trigger
        </a>

        <!-- Dropdown -->
        <div id="programmatic-dropdown" class="aui-style-default aui-dropdown2">
            <ul><li><a href="#">Dropdown item</a></li></ul>
        </div>
    </noscript>
</aui-docs-example>
<p>This is a toggle (for dropdown2 only), so if you dispatch "aui-button-invoke" on the trigger of an open dropdown, it will close.</p>

<h3>Events</h3>
<p>jQuery events "aui-dropdown2-show" and "aui-dropdown2-hide" are dispatched on the dropdown container element when it is shown and hidden.</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/js">
        AJS.$("#my-dropdown").on({
            "aui-dropdown2-show": function() {
                console.log('Dropdown shown');
            },
            "aui-dropdown2-hide": function(event) {
                console.log('Dropdown hidden');
            }
        });
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#" id="my-dropdown-trigger" aria-owns="my-dropdown" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">
            Dropdown trigger
        </a>

        <!-- Dropdown -->
        <div id="my-dropdown" class="aui-style-default aui-dropdown2">
            <ul><li><a href="#">Dropdown item</a></li></ul>
        </div>
    </noscript>
</aui-docs-example>

<p>Event handlers aren't able to prevent the default action of "aui-dropdown2-show" and "aui-dropdown2-hide" events. Add <code>class="interactive"</code> to the A elements that should not cause the dropdown to close when clicked.</p>
<p>jQuery events "aui-dropdown2-item-check" and "aui-dropdown2-item-uncheck" are dispatched on checkbox and radio A elements. Event handlers can be added with JavaScript:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/js">
        AJS.$("#spellcheck-dropdown").on({
            "aui-dropdown2-item-check": function() {
                document.querySelector('textarea').setAttribute('spellcheck', 'true');
                document.querySelector('#spellcheck-status').innerHTML = 'Spellcheck on';
            },
            "aui-dropdown2-item-uncheck": function() {
                document.querySelector('textarea').setAttribute('spellcheck', 'false');
                document.querySelector('#spellcheck-status').innerHTML = 'Spellcheck off';
            }
        });
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Trigger -->
        <a href="#" id="spellcheck-dropdown-trigger" aria-owns="spellcheck-dropdown" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">
            Options
        </a>

        <!-- Dropdown -->
        <div id="spellcheck-dropdown" class="aui-style-default aui-dropdown2">
            <div class="aui-dropdown2-section">
                <ul>
                    <li><a href="#" class="aui-dropdown2-checkbox interactive checked">Spellcheck</a></li>
                </ul>
            </div>
        </div>

        <!-- Textarea -->
        <span id="spellcheck-status">Spellcheck off</span>
        <p>
            <textarea></textarea>
        </p>
    </noscript>
</aui-docs-example>
