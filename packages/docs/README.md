# AUI documentation microsite

Documentation on how to use various patterns and components from the AUI library.

This documentation is hosted at [https://aui.atlassian.com/].
