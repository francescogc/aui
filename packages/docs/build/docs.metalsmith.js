const path = require('path');
const del = require('del');
const metalsmith = require('metalsmith');
const metalsmithLayouts = require('metalsmith-layouts');
const metalsmithInPlace = require('metalsmith-in-place');
const metalsmithMarkdown = require('metalsmith-markdown');
const metalsmithDefine = require('metalsmith-define');
const metalsmithRootpath = require('metalsmith-rootpath');
const metalsmithDiscoverHelpers = require('metalsmith-discover-helpers');
const metalsmithDiscoverPartials = require('metalsmith-discover-partials');
const metalsmithWatch = require('metalsmith-watch');

const pkg = require('@atlassian/aui/package.json');
const docsOpts = require('./docs.opts');
const libGetAuiVersions = require('./get-aui-versions');
const adg3colourMap = require(path.resolve(__dirname, '../src/assets/adg2-to-adg3-colours.json'));

function buildDocs(docsOpts, done) {
    const opts = Object.assign({}, docsOpts);
    const name = pkg.name;
    const version = opts.docsVersion || pkg.version;
    const { versions } = opts;

    const localDistUri = `//${opts.host}:${opts.port}/${opts.path}`;
    const cdnDistUri = `https://unpkg.com/${name}@${version}/dist/aui`;

    const distLocation = opts.localdist ? localDistUri : cdnDistUri;

    const OUT_DIR = path.resolve(__dirname, '../dist');
    const SRC_DIR = path.resolve(__dirname, '../');

    console.log('metalsmith opts', opts);

    const ms = metalsmith(SRC_DIR)
        .destination(OUT_DIR)
        // we rely on a higher process (gulp) to clean up.
        // that's because we use multiple processes (e.g., webpack) to build
        // the whole site.
        .clean(false)
        .use([
            metalsmithRootpath(),

            metalsmithDefine({
                version,
                versions,
                distLocation,
                adg3colourMap
            }),

            metalsmithMarkdown(),

            metalsmithDiscoverHelpers({
                directory: 'helpers',
                pattern: /\.js$/,
            }),

            metalsmithDiscoverPartials({
                directory: 'partials',
                pattern: /\.hbs$/,
            }),

            // Protip: If compilations bail because of errors like 'cannot find partial',
            // it may be because multiple versions of handlebars are being loaded, thus the partials
            // are not propagating from the "discover" plugins over to this plugin.
            // Ensure that there is only one handlebars version throughout this whole repo!
            metalsmithInPlace({
                pattern: '**/*.hbs'
            }),

            // Note: At this point, the docs files have a .html extension.

            metalsmithLayouts({
                directory: 'layouts',
                engine: 'handlebars',
                default: 'main-layout.hbs',
                pattern: '**/*.html'
            }),
        ]);

    if (opts.watch) {
        console.info('metalsmith will be watching...');
        ms.use([
            // The JavaScript and CSS assets are compiled via an external process (webpack).
            metalsmithWatch({
                paths: {
                    '${source}/**/*.hbs': true,
                    '${source}/**/*.html': true,
                    '${source}/**/*.json': true,
                    '${source}/**/*.png': true,
                }
            }),
        ]);
    }

    ms.build(function (err) {
        if (err) {
            throw new Error(err);
        }

        const dirs = [
            path.resolve(OUT_DIR, 'scripts'),
            path.resolve(OUT_DIR, 'styles')
        ];
        del(dirs).then(() => done(), done);
    });
}

module.exports = (buildOpts) => function buildDocsTemplates(done) {
    const whenBuildIsDone = (...args) => {
        console.info('metalsmith did its thing!', args);
        done(...args);
    };

    libGetAuiVersions()
        .then(versions => {
            const opts = Object.assign({}, docsOpts, { versions }, buildOpts);
            buildDocs(opts, whenBuildIsDone);
        })
        .catch(err => {
            console.error('hmm, something went wrong.', err);
            buildDocs(docsOpts, whenBuildIsDone);
        });
};
